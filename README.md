# Nuiton-J2R

Java library to invoke ``R`` native functions

## Build

To deploy new version of nuiton-j2r: 

  ``mvn deploy``

To install localy: 

  ``mvn install``

## rJava

### Windows

 * http://cran.r-project.org/bin/windows/contrib/2.10/rJava_0.6-2.zip
 * http://cran.r-project.org/bin/windows64/contrib/2.11/rJava_0.8-5.zip

### Linux

http://www.rforge.net/rJava/files/

  ``./configure``
  ``make``
