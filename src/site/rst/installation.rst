.. -
.. * #%L
.. * Nuiton Java-2-R
.. * %%
.. * Copyright (C) 2006 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

============
Installation
============

Le présent document a pour but de faciliter les différentes installations nécessaires en vue d'utiliser la librairie LutinJ2R.

.. contents::

Installation
============


Premièrement il est nécessaire d'avoir R d'installé sur la machine qui devra effectuer les calculs R. Chaque solution a des particularités.


Solution réseau
---------------


Il est possible d'utiliser R en local (machine locale) ou de confier les calculs à une tierce machine (machine distante).

Que ce soit sur la machine locale ou distante, il faut installer R. Le plus simple est de consulter la documentation en ligne de R :

::

  http://wiki.r-project.org/rwiki/doku.php?id=getting-started:installation:packages

La procédure suivante diffère selon la plateforme :


Sous Linux
~~~~~~~~~~


Il faut installer le serveur qui va réceptionner et traiter les requêtes TCP/IP. Dans l'ordre, il faut :


- Télécharger les sources de la version 0.4-3  (http://rosuda.org/Rserve/dist/Rserve_0.4-3.tar.gz) ou à defaut la dernière disponible à l'adresse  ;
- Se logguer en root ;
- Excécuter la commande "R CMD INSTALL Rserve_0.4-3.tar.gz".


A noter que l'étape d'installation requiert un compilateur C/C++. Une intervention supplémentaire est peut-être donc nécessaire !


Sous Windows
~~~~~~~~~~~~


Les étapes à suivre :


- Télécharger la version 0.4-3 précompilée (http://rosuda.org/Rserve/dist/w32bin/0.4-3/2.3.1/Rserve.exe) ou à défaut la dernière disponible à l'adresse http://rosuda.org/Rserve/dist/rserve-win.html ;
- Il faut ensuite copier le fichier téléchargé dans le dossier 'bin' de l'installation de R. Par défaut pour la version 2.3.1 de R, ce dossier est "C:\Program Files\R\R-2.3.1\bin".


Solution JNI
------------


Il faut avant tout que R soit installé. Le plus simple est de consulter la documentation en ligne de R :
  http://wiki.r-project.org/rwiki/doku.php?id=getting-started:installation:packages


Concrètement R est prêt.
Cependant, si la plateforme ou l'architecture de la machine est particulière, il reste à compiler les sources natives (non-Java) du projet et créer une librairie adaptée.

Heureusement, la procédure est simple :

Les sources à compiler font partie de l'archive *JRI-sources.zip* (http://lutinj2r.labs.libre-entreprise.org/libsys/JRI-sources.zip).

Pour compiler la librairie, dézippez dans un premier temps l'archive *JRI-sources.zip*. Ensuite, excécutez le script nommé "configure" de manière à préparer votre système à la compilation. Enfin, effectuez la compilation à l'aide de la commande "make".

La compilation effectuée, un fichier a été généré, dont le nom est propre à la plateforme (libjri.so, jri.dll, ...). Il ne reste plus qu'à configurer le système comme indiqué à l'étape suivante.

Des versions précompilées des librairies sont disponibles (http://lutinj2r.labs.libre-entreprise.org/libsys/)

Site web de JRI : http://www.rforge.net/JRI/


Configuration
=============


Solution réseau
---------------


Il n'y a de configuration à faire que s'il s'agit d'une utilisation sur une machine distante. Dans le cas contraire, la configuration de la solution réseau est terminée.

Dans le cas d'une machine distante, il faut autoriser les connections entrantes. Par défaut, Rserve rejette ces connections.

Pour contrer cela, il faut éditer un fichier de configuration.

Sous Linux, il s'agit du fichier

::

  /etc/Rserv.conf


alors que sous Windows, il s'agit du fichier

::

  Rserv.cfg - qui doit se trouver au même endroit que l'excécutable Rserve.exe. (Par défaut : C:\Program Files\R\R-2.3.1\bin)

Si ce fichier n'existe pas, il faut le créer et ajouter la ligne suivante :

::

  remote enable


Solution JNI
------------


La librairie compilée ou obtenue à l'étape précédente doit être placée au bon endroit sur le système pour pouvoir être utilisée.

Quelque soit la plateforme, il faut positionner des variables d'environnement.


- R_HOME : doit pointer sur le dossier d'installation de R (soit /usr/lib/R sous Linux ou C:\Program Files\R\R-(version) sous Windows ou ...)
- LD_LIBRARY_PATH : doit pointer sur le dossier lib contenu dans R_HOME (soit R_HOME/lib/ sous Linux ou R_HOME\lib sous Windows ou ...)
- PATH (pour Windows) : doit pointer sur le dossier d'installation de R (C:\Program Files\R\R-(version) ou ...) (en fait sur le dossier contenant la librairie R.dll)
Il faut ensuite copier la librairie (libjri.so ou jri.dll ou ...) dans le dossier LD_LIBRARY_PATH


Démarrage
=========


Cette étape n'est nécessaire que pour la solution réseau.


Solution réseau
---------------


La dernière étape de préparation est le lancement de Rserve.

Sous Linux, tapez la commande :

::

  R CMD Rserve

Sous Windows, double-cliquez sur :

::

  Rserve.exe dans le dossier d'installation de R (par défaut : C:\Program Files\R\R-2.3.1\bin)


A noter que vous pouvez aussi lancer Rserve à partir de R :

::

  library(Rserve)
  Rserve()
