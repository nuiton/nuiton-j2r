.. -
.. * #%L
.. * Nuiton Java-2-R
.. * %%
.. * Copyright (C) 2006 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. *
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. *
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=======================
Documentation du module
=======================

.. contents::

La librairie LutinJ2R est une passerelle d'accès à R. Ceci sous-entend qu'à
l'utilisation, il n'est pas nécessaire de se soucier de la technologie à employer
pour contacter R.


Utilisation
===========


Principes
---------


Pour accéder à R, l'utilisateur ne s'adresse qu'à un proxy auquel il revient de déterminer la technologie à employer.

Ce proxy est représenté par la classe *org.codelutin.j2r.RProxy*. La librairie LutinJ2R inclut plusieurs moyens d'accès à R.
Chacun de ses moyens est regroupé sous une interface commune qui définit les interactions possibles avec R : *org.codelutin.j2r.REngine*.

Le RProxy implante également cette interface, il s'utilise donc de la même manière.


Instanciation
-------------


La création du proxy est une simple instanciation.

::

  new RProxy();


Dans la mesure où la classe *RProxy* implante l'interface *REngine*, il est préférable de passer par cette interface :

::

  REngine engine;
  engine = new RProxy();

Note :

En fonction des cas, il peut être nécessaire de rajouter des import en début de classe :

::

  import org.codelutin.j2r.RProxy;
  import org.codelutin.j2r.REngine;

ou utiliser directement le nom complet des classes :

::

  org.codelutin.j2r.REngine engine;
  engine = new org.codelutin.j2r.RProxy();


Utilisation
-----------


Une fois que le proxy a été instancié, il est alors possible d'utiliser R.

Deux principales méthodes sont à disposition :


- **Object eval(String expr)** : Cette méthode prend une expression en paramètre sous forme de String, délègue le calcul à R et renvoie le résultat. Le type de retour est déterminé automatiquement et converti pour plus de simplicité. Ainsi si le résultat est un tableau de double, l'objet de retour sera un double[].
- **void voidEval(String expr)** : Cette méthode est la même que la précédente à ceci près qu'elle ne renvoie pas de résultat. Ceci permet d'économiser des temps de transfert et conversion lorsque ce n'est pas nécessaire. Par exemple, lors d'une assignation *t<-0*, il n'est pas nécessaire d'attendre de résultat. Cette fonctionnalité est surtout utile pour la solution réseau qui souffre de temps de transfert longs.

A noter que ces deux méthodes sont suceptibles de lever des exceptions de type *org.codelutin.j2r.RException* si un traitement a échoué.

Le mode d'utilisation est donc :

::

  engine.voidEval("t<-sin(0)");
  double d = (Double)engine.eval("t");


Paramétrage
===========


Par défaut, le proxy essaye de se connecter en réseau sur la machine locale. Néanmoins il peut être nécessaire de changer de technologie ou encore d'utiliser une machine distante.

Le paramétrage se fait par le biais d'une option au niveau de la JVM. Les options de la JVM permettent de positionner des paramètres qui ne sont pas directement liés à l'application mais plutôt à son environnement d'éxécution, comme la quantité de mémoire allouée, ...


Choix de la technologie
-----------------------


Pour informer l'application du type de technologie à utiliser, il faut rajouter l'option *R.type* :

::

  -DR.type=net : pour une utilisation par le réseau.
  -DR.type=jni : pour une utilisation pas JNI.

La détection de la technologie à employer est ensuite faite au niveau du *RProxy*.


Paramétres supplémentaires
--------------------------


Aucun paramètre supplémentaire ne peut être utile à JNI, cette section concerne donc la solution réseau.

Par défaut, la solution réseau s'adresse à la machine locale (127.0.0.1) et sur le port par défaut (6311), mais il est possible de spécifier une autre adresse de machine ou un autre port.

L'option de JVM peut être suffixé des manières suivantes :

::

  -DR.type=net://192.168.99.122 : De cette manière, la connection est établie sur la machine *192.168.99.122* sur le port par défaut.
  -DR.type=net://:9999 : De cette manière, la connection est établie sur la machine locale sur le port *9999*. Notez la présence du *:* supplémentaires.
  -DR.type=net://192.168.99.122:9999 : De cette manière, la connection est établie sur la machine *192.168.99.122* et sur le port *9999*. Notez la présence du *:* entre l'adresse et le port.

