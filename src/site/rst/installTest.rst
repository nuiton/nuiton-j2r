.. -
.. * #%L
.. * Nuiton Java-2-R
.. * %%
.. * Copyright (C) 2006 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Installer l'environnement de tests
==================================

Pour pouvoir effctuer les tests de nuiton-j2r, il faut un environnement
spécial : R doit être installé de même que Rserve, certaines variables
d'environnement doivent être déclarées et des librairies natives doivent être
utilisées.

Installation de R
-----------------

Pour installer R, référez-vous au site du projet : http://cran.r-project.org/

Pour information, sur les distributions linux de type debian/ubuntu, il faut
faire, en root, un petit ::

  apt-get install r-base-core

Installation de Rserve
----------------------

Pour installer Rserve, deux solutions sont possibles. Soit démarrer R (sous
linux par la commande R) puis lancer la commande ::

  install.packages("Rserve")

Soit, pour les utilisateurs linux, installer par les packets, soit pour
debian/ubuntu, en root ::

  apt-get install r-cran-rserve

Installation de JRI
-------------------

Pour installer JRI, le processus est plus complexe. Soit vous pouvez vous
satisfaire des versions précompilées, soit vous devrez compiler la librairie
pour votre système.

Installation sans compilation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans le dossier src/main/resources, vous pourrez trouver la librairie jri
précompilée pour la plupart des environnements. Vous pouvez la recompiler si
besoin, ou si les version précompilées ne fonctionnent pas.

Copiez le fichier correspondant à votre système dans le dossier que vous
souhaitez.

Passez maintenant à la phase de configuration

Installation avec compilation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour compiler la librairie, dézippez dans un premier temps l'archive
*JRI-sources.zip*. Ensuite, excécutez le script nommé "configure" de manière à
préparer votre système à la compilation. Enfin, effectuez la compilation à
l'aide de la commande "make".

La compilation effectuée, un fichier a été généré, dont le nom est propre à la
plateforme (libjri.so, jri.dll, ...).

Copiez le fichier correspondant à votre système dans le dossier que vous
souhaitez.

Passez maintenant à la phase de configuration.

Configuration
~~~~~~~~~~~~~

Vous devez modifier vos variables d'environnement :

- R_HOME : doit pointer sur le dossier d'installation de R (soit /usr/lib/R sous
  Linux ou C:\Program Files\R\R-(version) sous Windows ou ...)
- LD_LIBRARY_PATH : doit pointer sur le dossier lib contenu dans R_HOME (soit
  R_HOME/lib/ sous Linux ou R_HOME\lib sous Windows ou ...)
- PATH (pour Windows) : doit pointer sur le dossier d'installation de R
  (C:\Program Files\R\R-(version) ou ...) (en fait sur le dossier contenant la
  librairie R.dll)

Lancer les tests
================

Il faut lancer un serveur Rserve, en lançant la commande ::

  R CMD Rserve

Tout doit maintenant être prêt et vous pouvez faire tourner les tests de
nuiton-j2r.
