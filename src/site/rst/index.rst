.. -
.. * #%L
.. * Nuiton Java-2-R
.. * %%
.. * Copyright (C) 2006 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. *
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. *
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

==========
Nuiton-J2R
==========

La librairie Nuiton-J2R a pour but de fournir une passerrelle unifiée d'accès à R.

Pour exécuter les tests, ne pas oublier de lancer Rserve (R CMD Rserve) et
l'argument suivant : -DargLine="-Djava.library.path=/usr/local/lib" au lancement
du build. Il indique l'emplacement de la librairie libjri.so.
