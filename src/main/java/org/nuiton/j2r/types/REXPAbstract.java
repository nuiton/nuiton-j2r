/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r.types;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.REngine;
import org.nuiton.j2r.RException;
import org.nuiton.j2r.RInstructions;

/**
 * Abstract class for REXP interface, in order to avoid duplicate code for attributes related methods.
 *
 * @author couteau
 */
public abstract class REXPAbstract implements REXP {

    private Log log = LogFactory.getLog(REXPAbstract.class);

    //Name of the data.frame in R
    protected String variable;

    //Attributes of the data.frame under a map -> name : R expression
    protected Map<String, Object> attributes;

    //Engine used for R instructions.
    protected REngine engine;

    //Vector containing the names of the data.frame vectors
    protected List<String> names;
    protected String indexExceptionText =
            "Cannot perform operation, index is superior to size.\nIndex : %s\nSize : %s";
    protected String dataInconsistencyText =
            "There is an inconsistency between the local and distant data.\nLocal data size : %s\nDistant data size : %s";
    protected String noVariable = "No variable name given";

    /** {@inheritDoc} */
    @Override
    public void setAttributes(Map<String, Object> attributes) throws RException {

        if (attributes!=null){
            for(Map.Entry<String, Object> entry : attributes.entrySet()){
                setAttribute(entry.getKey(),entry.getValue());
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public Map<String, Object> getAttributes() throws RException {
        if (engine.isAutoCommit()) {
            //get the number of attributes
            Integer attributeslength = (Integer) engine.eval(String.format(
                    RInstructions.LENGTH_ATTRIBUTES, this.variable));

            for (int i = 0; i < attributeslength; i++) {
                //Get the name of the i attribute
                String key = (String) engine.eval(String.format(
                        RInstructions.GET_ATTRIBUTE_NAME, this.variable, i + 1));

                //Get the attribute
                getAttribute(key);
            }
        }
        return this.attributes;
    }

    /** {@inheritDoc} */
    @Override
    public Object getAttribute(String attribute) throws RException {
        if (engine.isAutoCommit()) {
            Object returnedAttribute;
            //test if the attribute is a data.frame
            if ((Boolean) engine.eval("is.data.frame(" + String.format(
                    RInstructions.GET_ATTRIBUTE, this.variable, attribute) + ")")) {
                //if attribute is a list, import the data.frame from R
                returnedAttribute = new RDataFrame(engine);
                ((RDataFrame) returnedAttribute).getFrom(String.format(
                        RInstructions.GET_ATTRIBUTE, this.variable, attribute));
            //test if the attribute is a list
            } else if ((Boolean) engine.eval("is.list(" + String.format(
                    RInstructions.GET_ATTRIBUTE, this.variable, attribute) + ")")) {
                //if attribute is a list, import it from R.
                returnedAttribute = new RList(engine);
                ((RList) returnedAttribute).getFrom(String.format(
                        RInstructions.GET_ATTRIBUTE, this.variable, attribute));
            } else {
                //else attribute is imported as any other R expression.
                returnedAttribute = engine.eval(String.format(
                        RInstructions.GET_ATTRIBUTE, this.variable, attribute));
            }

            //put the attribute in the attribute list
            if (returnedAttribute != null) {
                if (attributes.containsKey(attribute)) {
                    //if the attribute already exists, remove it first (or you
                    //will have too times the same attribute with different
                    //values)
                    attributes.remove(attribute);
                    attributes.put(attribute, returnedAttribute);
                } else {
                    attributes.put(attribute, returnedAttribute);
                }
            } else {
                throw new RException("Attribute does not exist");
            }
        } else if (!attributes.containsKey(attribute)) {
            //if no autocommit and attribute does not exist
            throw new RException("Attribute does not exist");
        }
        return attributes.get(attribute);
    }

    /** {@inheritDoc} */
    @Override
    public void setAttribute(String attribute, Object value) throws RException {
        if (value instanceof String) {
            setAttribute(attribute, (String)value);
        } else if (value instanceof Double) {
            setAttribute(attribute, (Double)value);
        } else if (value instanceof Integer) {
            setAttribute(attribute, (Integer)value);
        } else if (value instanceof Boolean) {
            setAttribute(attribute,(Boolean)value);
        } else if (value instanceof REXP) {
            setAttribute(attribute, (REXP)value);
        } else {
            log.warn("This attribute is not valid : " + attribute + " ; " +
                    value + ". It will not be processed");
        }
    }

    public void setAttribute(String attribute, Double value) throws RException {
        engine.voidEval(String.format(RInstructions.SET_ATTRIBUTE,
                this.variable, attribute, value));
        putAttributeInData(attribute,value);
    }

    public void setAttribute(String attribute, Integer value) throws RException {
        //If integer in the R function : as.integer()
        engine.voidEval(String.format(RInstructions.SET_ATTRIBUTE,
                this.variable, attribute, String.format(
                        RInstructions.AS_INTEGER, value)));
        putAttributeInData(attribute,value);
    }

    public void setAttribute(String attribute, Boolean value) throws RException {
        //R boolean is upper case
        engine.voidEval(String.format(RInstructions.SET_ATTRIBUTE,
                this.variable, attribute, value.toString().toUpperCase()));
        putAttributeInData(attribute,value);
    }

    public void setAttribute(String attribute, String value) throws RException {
        //if String, between quotes
        engine.voidEval(String.format(RInstructions.SET_ATTRIBUTE,
                this.variable, attribute, "\"" + value + "\""));
        putAttributeInData(attribute,value);
    }

    public void setAttribute(String attribute, REXP value) throws RException {
        //if REXP, use its method toRString()
        engine.voidEval(String.format(RInstructions.SET_ATTRIBUTE,
                this.variable, attribute, value.toRString()));
        putAttributeInData(attribute,value);
    }

    private void putAttributeInData(String attribute, Object value){
        if (attributes.containsKey(attribute)) {
            attributes.remove(attribute);
            attributes.put(attribute, value);
        } else {
            attributes.put(attribute, value);
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getVariable() {
        return this.variable;
    }

    /**
     * Method to set the R expression variable name
     *
     * @param variable
     *            the new variable name
     * @throws RException
     *             if something wrong happen while assigning the R expression to
     *             the new variable in R.
     */
    public void setVariable(String variable) throws RException {
        this.variable = variable;
        try {
            engine.voidEval(toRString());
        } catch (RException eee) {
            throw new RException(
                    "An error occured while trying to assign the list in R",
                    eee);
        }
    }

    /**
     * Method to get the names of the ArrayLists of the R data.frame.
     *
     * @return a ArrayList of strings containing the names of each ArrayList of
     *         the R data.frame
     * @throws RException
     *             if an error occurs while getting back the names from R.
     * @throws IndexOutOfBoundsException
     *             when the row.names size get from R is bigger than the local
     *             data size.
     */
    public List<String> getNames() throws RException {

        if (engine.isAutoCommit()) {
            //Get back the names from R.
            String[] namesArray = (String[]) engine.eval(String.format(
                    RInstructions.GET_NAMES, this.variable));

            //Check if size is correct, if yes, modify local data.
            checkX(namesArray.length);
            this.names.clear();
            names.addAll(Arrays.asList(namesArray));
            return names;

        } else {
            return this.names;
        }

    }

    /**
     * Method to get the name of the ArrayList that is at this x index of the R
     * data.frame.
     *
     * @param x index of the ArrayList (0 to n-1)
     *
     * @return the name of the ArrayList
     *
     * @throws RException
     *             if an error occurs while getting back the name from R.
     *
     * @throws IndexOutOfBoundsException
     *             if the x index is out of bounds.
     */
    public String getName(int x) throws RException {

        //check if the index is valid
        if (x < names.size()) {
            if (engine.isAutoCommit()) {
                //Get back the names from R.
                String name = (String) engine.eval(String.format(
                        RInstructions.GET_NAME, this.variable, x + 1));

                //Check if the String is returned.
                if ((name != null) && (!name.equals(""))) {
                    this.names.set(x, name);
                }
            }
        } else {
            //if String is not returned, throw an IndexOutOfBOundsException.
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    x, this.names.size()));
        }

        return this.names.get(x);

    }

    /**
     * Test that the variable name has been set so that the REXP can be sent to
     * R.
     *
     * @throws org.nuiton.j2r.RException if the variable name have not been set.
     */
    protected void checkVariable() throws RException {
        if ((this.variable == null) || (this.variable.equals(""))) {
            throw new RException(
                    noVariable);
        }
    }

    /**
     * Method to set the engine used by the REXP. The REXP is also sent to the
     * engine.
     *
     * @param engine
     *            Engine to be set
     * @throws RException
     *             If an error occur while assigning the list to the new
     *             REngine.
     */
    public void setEngine(REngine engine) throws RException {
        this.engine = engine;
        try {
            engine.eval(toRString());
        } catch (RException eee) {
            throw new RException(
                    "Cannot assign the data.frame to the new REngine", eee);
        }
    }

    /**
     * Method to get the engine used by the REXP
     *
     * @return the engine used.
     */
    public REngine getEngine() {
        return engine;
    }

    /**
     * Method to assign names of the complex REXP objects.
     *
     * @param names
     *            a ArrayList containing the names of the REXP.
     * @throws RException
     *             when the names list is longer than the REXP length.
     */
    public void setNames(List<String> names) throws RException {

        //Check if the size is correct, if yes, do the modifications and send them to R.
        checkX(names.size());

        //Now we know we do not have a size problem.
        this.names = names;

        //Create the r instruction (names(var)<-c("name 1",...,"name x")).
        StringBuilder namesString = new StringBuilder("");
        for (int i = 0; i < this.names.size(); i++) {
            if (i != 0) {
                namesString.append(",");
            }
            namesString.append("\"");
            namesString.append(names.get(i));
            namesString.append("\"");
        }
        String rexp = String.format(RInstructions.SET_NAMES, this.variable,
                namesString);

        //Send the r instruction to the engine.
        engine.voidEval(rexp);
    }

    /**
     * Method to assign the name of the Object at the index x.
     *
     * @param x
     *            index of the object (0 to n-1)
     * @param name
     *            Name of the object.
     * @throws RException
     *             if an error occur while in R
     */
    public void setName(int x, String name) throws RException {
        //check if the index is valid
        checkX(x);
        //Now, index is valid.
        for (int i = 0; i <= x; i++) {
            try {
                names.get(i);
                if (x == i) {
                    names.set(x, name);
                }
            } catch (IndexOutOfBoundsException eee) {
                if (x == i) {
                    names.add(name);
                } else {
                    names.add(null);
                }
            }

        }

        //create the r instruction (names(var)[x]<-"name")
        String rexp = String.format(RInstructions.SET_NAME, this.variable, x + 1,
                name);

        //Send the R instruction to the engine.
        engine.voidEval(rexp);
    }
}
