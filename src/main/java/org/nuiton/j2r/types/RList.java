/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.REngine;
import org.nuiton.j2r.RException;
import org.nuiton.j2r.RInstructions;

/**
 * Java implementation of the R List.
 * 
 * @author couteau
 */
public class RList extends REXPAbstract implements REXP {

    //Content of the list (eg elements of the list in R)
    protected List<Object> data;
    protected Log log = LogFactory.getLog(RDataFrame.class);
    /**
     * Create a default RList linked to a R engine (the List is not initialized
     * in R.)
     *
     * @param engine the R engine in which to assign the RList.
     */
    public RList(REngine engine) {
        super();
        this.names = new ArrayList<String>();
        this.data = new ArrayList<Object>();
        this.variable = "";
        this.engine = engine;
        this.attributes = new HashMap<String, Object>();
    }

    /**
     *  Create the RList with parameters and initialize it in R.
     *
     * @param names the names of each object of the list.
     * @param data the list of the objects that compound the RList.
     * @param engine the R engine in which to assign the RList.
     * @param variable the variable name in R.
     * @throws org.nuiton.j2r.RException if an error occur while initializing
     * the list in R.
     */
    public RList(List<String> names, List<Object> data, REngine engine,
            String variable) throws RException {
        super();
        this.names = names;
        this.data = data;
        this.variable = variable;
        this.attributes = new HashMap<String, Object>();
        this.engine = engine;
        try {
            engine.voidEval(this.toRString());
        } catch (RException eee) {
            throw new RException("Cannot initialize list in R", eee);
        }
    }

    /**
     *  Create the RList with parameters and initialize it in R.
     *
     * @param names the names of each object of the list.
     * @param data the list of the objects that compound the RList.
     * @param engine the R engine in which to assign the RList.
     * @param variable the variable name in R.
     * @throws org.nuiton.j2r.RException if an error occur while initializing
     * the list in R.
     */
    public RList(String[] names, List<Object> data, REngine engine,
            String variable) throws RException {

        super();

        String[] tempNames = {};

        if(names!=null){
            tempNames = names.clone();
        }


        this.names = new ArrayList<String>();
        this.names.addAll(Arrays.asList(tempNames));
        this.data = data;
        this.variable = variable;
        this.attributes = new HashMap<String, Object>();
        this.engine = engine;
        try {
            engine.eval(this.toRString());
        } catch (RException eee) {
            //As for autoconversion,there is no variable name, this will always
            //be thrown. Catch it and log it instead so that it does not change
            //the behaviour and we can use this method with and without variable
            //name
            if (log.isDebugEnabled()) {
                log.debug("Cannot initialize data.frame in R : " +
                        eee.getMessage());
            }
        }
    }

    /**
     * Method to export the list in a String for evaluation in R.
     * 
     * @return a R string representation of the list to create it in R.
     * @throws RException
     *             If no variable name is given
     */
    @Override
    public String toRString() throws RException {
        checkVariable();
        StringBuilder returnString = new StringBuilder();
        returnString.append(this.variable);
        returnString.append("<-list(");
        if ((this.data != null) && (!(this.data.isEmpty()))) {
            for (int i = 0; i < data.size(); i++) {
                returnString.append(toRString(i));
            }
            returnString = new StringBuilder(returnString.substring(0, returnString.length() - 1));
        }
        returnString.append(")");

        if (log.isDebugEnabled()){
            log.debug(returnString);
        }

        return returnString.toString();
    }

    /**
     * Create the R instruction for element at index i
     * @param i index
     * @return the corresponding R instruction
     * @throws RException if an error occur creating R instruction for REXPs
     */
    protected String toRString(int i) throws RException {
        String returnString="";

        Object obj = data.get(i);

        if (!(this.names.isEmpty())) {
            returnString += this.names.get(i) + "=";
        }
        
        if (obj instanceof String) {
            returnString += "\"" + obj + "\",";
        } else if ((obj instanceof Boolean) && ((Boolean) obj)) {
            returnString += RInstructions.TRUE + ",";
        } else if ((obj instanceof Boolean) && (!(Boolean)obj)) {
            returnString += RInstructions.FALSE + ",";
        } else if (obj instanceof Integer) {
            returnString += String.format(RInstructions.AS_INTEGER, obj) + ",";
        } else if (obj instanceof REXP) {
            returnString += ((REXP)obj).toRString() + ",";
        } else {
            returnString += obj + ",";
        }

        return returnString;
    }

    /**
     * Method to set the xth object of the list to a value.
     * 
     * @param x
     *            x coordinate (from 0 to size-1)
     * @param data
     *            value to set
     * @throws RException
     *             if no variable name has been given to the list
     */
    public void set(int x, Double data) throws RException {
        checkVariable();

        setInData(x,data);

        //in R, all data is numeric, no need to transformation, simple
        //assignation
        engine.voidEval(String.format(RInstructions.SET_LIST_ITEM,
                this.variable, x + 1, data));
    }

    /**
     * Method to set the xth object of the list to a value.
     *
     * @param x
     *            x coordinate (from 0 to size-1)
     * @param data
     *            value to set
     * @throws RException
     *             if no variable name has been given to the list
     */
    public void set(int x, Integer data) throws RException {
        checkVariable();

        setInData(x,data);

        //transform the data in R integer
        String asInteger = String.format(RInstructions.AS_INTEGER, data);
        //assign the transformed data
        engine.voidEval(String.format(RInstructions.SET_LIST_ITEM,
                this.variable, x + 1, asInteger));
    }

    /**
     * Method to set the xth object of the list to a value.
     *
     * @param x
     *            x coordinate (from 0 to size-1)
     * @param data
     *            value to set
     * @throws RException
     *             if no variable name has been given to the list
     */
    public void set(int x, Boolean data) throws RException {
        checkVariable();

        setInData(x,data);

        if (data) {
            engine.voidEval(String.format(RInstructions.SET_LIST_ITEM,
                    this.variable, x + 1, RInstructions.TRUE));
        } else {
            engine.voidEval(String.format(RInstructions.SET_LIST_ITEM,
                    this.variable, x + 1, RInstructions.FALSE));
        }
    }

    /**
     * Method to set the xth object of the list to a value.
     *
     * @param x
     *            x coordinate (from 0 to size-1)
     * @param data
     *            value to set
     * @throws RException
     *             if no variable name has been given to the list
     */
    public void set(int x, String data) throws RException {
        checkVariable();

        setInData(x,data);

        engine.voidEval(String.format(RInstructions.SET_LIST_ITEM,
                this.variable, x + 1, "\"" + data + "\""));
    }

    /**
     * Method to set the xth object of the list to a value.
     *
     * @param x
     *            x coordinate (from 0 to size-1)
     * @param data
     *            value to set
     * @throws RException
     *             if no variable name has been given to the list
     */
    public void set(int x, REXP data) throws RException {
        checkVariable();

        setInData(x,data);

        //Send the REXP to R
        engine.voidEval(data.toRString());
        //Set the REXP as list item
        engine.voidEval(String.format(RInstructions.SET_LIST_ITEM,
                this.variable, x + 1, data.getVariable()));
    }

    
    protected void setInData(int x, Object data){
        for (int i = 0; i <= x; i++) {
            try {
                this.data.get(i);
                if (x == i) {
                    this.data.set(x, data);
                }
            } catch (IndexOutOfBoundsException eee) {
                if (x == i) {
                    this.data.add(data);
                } else {
                    this.data.add(null);
                }
            }
        }
    }

    /**
     * Method to get the object located at the [x] coordinate.
     * 
     * @param x
     *            x coordinates (between 0 and size-1)
     * @return the Object located at the [x] coordinate
     * @throws RException
     *             if no variable name has been given to the list and tries to
     *             retrieve data from R
     */
    public Object get(int x) throws RException {
        checkX(x);
        if (engine.isAutoCommit()) {
            checkVariable();
            Object returnObject = engine.eval(String.format(
                    RInstructions.GET_LIST_ITEM, this.variable, x + 1));
            if (returnObject instanceof String) {
                this.data.set(x, returnObject);
            } else if (returnObject instanceof Double) {
                this.data.set(x, returnObject);
            } else if (returnObject instanceof Integer) {
                this.data.set(x, returnObject);
            } else if (returnObject instanceof Boolean) {
                this.data.set(x, returnObject);
            }
        }
        return this.data.get(x);
    }

    /**
     * Method to get the elements of the R list (there is no synchronizing with
     * R).
     * 
     * @return a List containing the elements of the R list.
     */
    public List<Object> getData() {
        return data;
    }

    /**
     * Method to assign the data of the R list.
     * 
     * @param data
     *            a List of Objects, containing each element of the R list
     * @throws RException if cannot communicate with R
     */
    public void setData(List<Object> data) throws RException {
        this.data = data;
        engine.voidEval(toRString());
    }

    /**
     * Method to get a list from a variable in R.
     * 
     * @param variable
     *            name of the data.frame in R
     * @throws RException
     */
    @Override
    public void getFrom(String variable) throws RException {
        this.variable = variable;
        if (names != null) {
            names.clear();
        } else {
            names = new ArrayList<String>();
        }
        if (data != null) {
            data.clear();
        } else {
            data = new ArrayList<Object>();
        }
        if (attributes != null) {
            attributes.clear();
        } else {
            attributes = new HashMap<String, Object>();
        }

        //update names
        String[] namesArray = (String[]) engine.eval(String.format(
                RInstructions.GET_NAMES, this.variable));
        names.addAll(Arrays.asList(namesArray));

        //update data
        int length = (Integer) engine.eval(String.format(RInstructions.LENGTH,
                variable));
        for (int i = 0; i < length; i++) {
            data.add(engine.eval(String.format(RInstructions.GET_LIST_ITEM,
                    this.variable, i + 1)));
        }

        //update attributes
        Integer attributeslength = (Integer) engine.eval(String.format(
                RInstructions.LENGTH_ATTRIBUTES, this.variable));

        for (int i = 0; i < attributeslength; i++) {
            String key = (String) engine.eval(String.format(
                    RInstructions.GET_ATTRIBUTE_NAME, this.variable, i + 1));

            Object attribute = engine.eval(String.format(
                    RInstructions.GET_ATTRIBUTE, this.variable, key));
            attributes.put(key, attribute);
        }
    }

    /**
     * Method that check if the index is inside the REXP bounds.
     * @param x index
     */
    @Override
    public void checkX(int x) {
        if (x > this.data.size()) {
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    x, this.data.size()));
        }
    }

    @Override
    public String toString(){

        StringBuilder returnString = new StringBuilder("");

        for(int i=0;i<this.data.size();i++){

            //display the index
            if( (names!=null) && (!names.isEmpty()) && (names.get(i)!=null)){
                returnString.append("[[");
                returnString.append(names.get(i));
                returnString.append("]]\n");
            } else {
                returnString.append("[[");
                returnString.append(i);
                returnString.append("]]\n");
            }

            //display the item at index i
            returnString.append(this.data.get(i));
            returnString.append("\n\n");
        }
        return returnString.toString();
    }
}
