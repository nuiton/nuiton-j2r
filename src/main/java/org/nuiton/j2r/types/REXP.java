/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r.types;

import java.util.Map;

import org.nuiton.j2r.RException;

/**
 * This interface is the common part of all complex R types.
 *
 * @author couteau
 */
public interface REXP {

    /**
     * Method to export the R object in a String for evaluation in R.
     * 
     * @return a String to create the R object in R.
     */
    String toRString() throws RException;

    /**
     * Method to get a data.frame from a variable in R.
     * 
     * @param variable
     *            name of the data.frame in R
     * @throws RException
     */
    void getFrom(String variable) throws RException;

    /**
     * Method to set all the attributes of the data.frame (there is no
     * synchronizing with R, use the commit() method to send data to R.
     * 
     * @param attributes
     *            a Map containing the attributes (key) and values (value)
     *            (values are a R expression, String may be rounded with escaped
     *            quote like : \"this is a R string\" ).
     */
    void setAttributes(Map<String, Object> attributes) throws RException;

    /**
     * Method to get all the attributes of the data.frame (there is no
     * synchronizing with R, use the update() method to synchronize data with R
     * before using this method if you think data may have changed.
     * 
     * @return a Map containing the attributes (key) and values (value)
     */
    Map<String, Object> getAttributes() throws RException;

    /**
     * Method to get the value of an attribute (there is no synchronizing with
     * R, use the update() method to synchronize data with R before using this
     * method if you think data may have changed.
     * 
     * @param attribute
     *            name of the attribute
     * @return the attribute value
     */
    Object getAttribute(String attribute) throws RException;

    /**
     * Method to set the value of an attribute (there is no synchronizing with
     * R, use the commit() method to send data to R.
     * 
     * @param attribute
     *            name of the attribute
     * @param value
     *            the value to be set (this is a R expression, String may be
     *            rounded with escaped quote like : \"this is a R string\" ).
     */
    void setAttribute(String attribute, Object value) throws RException;

    /**
     * Method to get the variable name of the REXP
     * 
     * @return the variable name of the REXP in R
     */
    String getVariable();

    /**
     * Method that check if the index is inside the REXP bounds.
     * @param x index
     */
    void checkX(int x);
}
