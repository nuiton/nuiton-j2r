/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r.types;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.REngine;
import org.nuiton.j2r.RException;
import org.nuiton.j2r.RInstructions;

/**
 * Java implementation of the R data.frame
 *
 * @author couteau
 */
public class RDataFrame extends REXPAbstract implements REXP {

    private Log log = LogFactory.getLog(RDataFrame.class);
    
    //Vector containing the names of the rows of the data.frame vectors
    private List<String> rowNames;
    //Vector containing the vectors of the data.frame
    private List<List<?>> data;

    /**
     * Constructor
     *
     * @param engine the engine in which the data frame will be located.
     */
    public RDataFrame(REngine engine) {
        super();
        this.names = new ArrayList<String>();
        this.rowNames = new ArrayList<String>();
        this.data = new ArrayList<List<?>>();
        this.variable = "";
        this.engine = engine;
        this.attributes = new HashMap<String, Object>();
    }

    /**
     * Constructor
     *
     * @param engine the engine in which the data frame will be located.
     * @param datatypes a table object determining the type of each column of
     * the data.frame.
     * @param y the length of each vector that compound the data.frame.
     *
     * @throws RException if a datatype is not supported
     */
    public RDataFrame(REngine engine, Object[] datatypes, int y) throws
            RException {
        super();
        this.names = new ArrayList<String>();
        this.rowNames = new ArrayList<String>();
        this.data = new ArrayList<List<?>>();
        for (Object datatype : datatypes) {
            //create one column for each datatype element
            //check if type is supported
            checkType(datatype);
            //create the list of objects
            List<Object> column = new ArrayList<Object>();
            //Fill it with instances of the datatype
            for (int j = 0; j < y; j++) {
                if (datatype instanceof Double) {
                    column.add(0.0);
                } else if (datatype instanceof Integer) {
                    column.add(0);
                } else if (datatype instanceof Boolean) {
                    column.add(true);
                } else if (datatype instanceof String) {
                    column.add("");
                }
            }
            //add the column to the dataframe.
            data.add(column);
        }
        this.variable = "";
        this.engine = engine;
        this.attributes = new HashMap<String, Object>();
    }

    /**
     * Constructor
     *
     * @param engine the engine in which the data frame will be located.
     * @param names names of the the data.frame columns.
     * @param rowNames names of the data.frame rows.
     * @param data the data of the data.frame
     * @param variable the name of the data.frame in R.
     * @throws org.nuiton.j2r.RException if an error occur while trying to
     * initialize 
     */
    public RDataFrame(REngine engine, List<String> names,
            List<String> rowNames, List<List<?>> data,
            String variable) throws RException {
        super();
        this.names = names;
        this.rowNames = rowNames;
        this.data = data;
        this.variable = variable;
        this.engine = engine;
        this.attributes = new HashMap<String, Object>();
        try {
            engine.eval(this.toRString());
        } catch (RException eee) {
            throw new RException("Cannot initialize data.frame in R", eee);
        }
    }

    /**
     * Constructor, used for autoconversion from rexp to data.frame.
     *
     * @param engine the engine in which the data frame will be located.
     * @param names names of the the data.frame columns.
     * @param rowNames names of the data.frame rows.
     * @param data the data of the data.frame
     * @param variable the name of the data.frame in R.
     */
    public RDataFrame(REngine engine, String[] names,
            String[] rowNames, List<List<?>> data,
            String variable) {
        //Create the name ArrayList and fill it with the content of the name String[]
        String[] tempNames = {};
        if (names != null) {
            tempNames = names.clone();
        }
        String[] tempRowNames = {};
        if (rowNames != null) {
            tempRowNames = rowNames.clone();
        }
        this.names = new ArrayList<String>();
        this.names.addAll(Arrays.asList(tempNames));
        //Create the rowNames ArrayList and fill it with the content of the rowNames
        //String[]
        this.rowNames = new ArrayList<String>();
        this.rowNames.addAll(Arrays.asList(tempRowNames));
        this.data = data;
        this.variable = variable;
        this.engine = engine;
        this.attributes = new HashMap<String, Object>();
        try {
            engine.eval(this.toRString());
        } catch (RException eee) {
            //As for autoconversion,there is no variable name, this will always
            //be thrown. Catch it and log it instead so that it does not change
            //the behaviour and we can use this method with and without variable
            //name
            if (log.isDebugEnabled()) {
                log.debug("Cannot initialize data.frame in R : " +
                        eee.getMessage());
            }
        }
    }

    /**
     * Method to get the names of the rows of the R data.frame
     * 
     * @return a ArrayList of strings containing the names of each row of the R
     *         data.frame
     * @throws RException
     *             if an error occurs while getting back the names from R.
     * @throws IndexOutOfBoundsException
     *             when the row.names size get from R is bigger than the local
     *             data size.
     */
    public List<String> getRowNames() throws RException {
        if (engine.isAutoCommit()) {
            //Get back the names from R.
            String[] rowNamesArray = (String[]) engine.eval(String.format(
                    RInstructions.GET_ROW_NAMES, this.variable));

            //Check if size is correct, if yes, modify local data.
            if (rowNamesArray.length <= this.data.get(0).size()) {
                this.rowNames.clear();
                rowNames.addAll(Arrays.asList(rowNamesArray));
                return rowNames;
            } else {
                //if size is not correct, throw a RException.
                throw new IndexOutOfBoundsException(String.format(
                        dataInconsistencyText, rowNamesArray.length,
                        this.data.get(0)));
            }
        } else {
            return this.rowNames;
        }

    }

    /**
     * Method to get the row name of the row index y+1 of the R data.frame.
     * 
     * @param y
     *            index of the row (0 to n-1)
     * 
     * @return the name of the ArrayList
     * 
     * @throws RException
     *             if an error occurs while getting back the row name from R.
     */
    public String getRowName(int y) throws RException {
        //check if the index is valid
        if (y < rowNames.size()) {
            if (engine.isAutoCommit()) {
                //Get back the names from R.
                String name = (String) engine.eval(String.format(
                        RInstructions.GET_ROW_NAME, this.variable, y + 1));

                //Check if the String is returned.
                if ((name != null) && (!name.equals(""))) {
                    this.rowNames.set(y, name);
                }
            }
        } else {
            //if String is not returned, throw an IndexOutOfBOundsException.
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    y, rowNames.size()));
        }
        return this.rowNames.get(y);

    }

    /**
     * Method to assign the names of the rows of the R data.frame
     * 
     * @param rowNames
     *            a ArrayList containing the names of the rows of the R
     *            data.frame
     * @throws RException
     *             if an error occur while in R.
     * 
     * @throws IndexOutOfBoundsException
     *             when the row names ArrayList is longer than the data.frame
     *             length
     */
    public void setRowNames(List<String> rowNames) throws RException {

        //Check if the size is correct, if yes, do the modifications and send them to R.
        if (rowNames.size() == data.get(0).size()) {
            this.rowNames = rowNames;

            //Create the r instruction (row.names(var)<-c("name 1",...,"name x")).
            StringBuilder rowNamesString = new StringBuilder();
            for (int i = 0; i < this.rowNames.size(); i++) {
                if (i != 0) {
                    rowNamesString.append(",");
                }
                rowNamesString.append("\"");
                rowNamesString.append(rowNames.get(i));
                rowNamesString.append("\"");
            }
            String rexp = String.format(RInstructions.SET_ROW_NAMES,
                    this.variable, rowNamesString.toString());

            //Send the r instruction to the engine.
            engine.voidEval(rexp);
        } else {
            //if size is not correct, throw a RException.
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    rowNames.size(), data.get(0).size()));
        }
    }

    /**
     * Method to assign the name of the ArrayList at the index x.
     * 
     * @param x
     *            index of the ArrayList (0 to n-1)
     * @param rowName
     *            Name of the ArrayList.
     * @throws RException
     *             if an error occur while in R.
     * @throws IndexOutOfBoundsException
     *             when the index is out of the data.frame bounds.
     */
    public void setRowName(int x, String rowName) throws RException {
        //check if the index is valid
        if (x < rowNames.size()) {
            rowNames.set(x, rowName);

            //create the r instruction (row.names(var)[x]<-"rowName")
            String rexp = String.format(RInstructions.SET_ROW_NAME,
                    this.variable, x + 1, rowName);

            //Send the R instruction to the engine.
            engine.voidEval(rexp);
        } else {
            //if index is out of bounds, throw a RException
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    x, data.size()));
        }
    }

    /**
     * Method to export the data.frame in a String for evaluation in R.
     * 
     * @return a R string representation of the data.frame to create it in R.
     * @throws RException
     *             If no variable name is given
     */
    @Override
    public String toRString() throws RException {
        checkVariable();

        StringBuilder returnString = new StringBuilder();

        returnString.append(this.variable);
        returnString.append("<-data.frame(");

        for (List<?> column:this.data){

            if (!column.isEmpty()){

                if (!(this.names.isEmpty())) {
                    int index = this.data.indexOf(column);
                    returnString.append(this.names.get(index));
                    returnString.append("=c(");
                } else {
                    returnString.append("c(");
                }

                Object firstElement = column.get(0);


                if (firstElement instanceof String) {
                    for (Object obj:column) {
                        returnString.append("\"");
                        returnString.append(obj);
                        returnString.append("\",");
                    }
                } else if (firstElement instanceof Boolean) {
                    for (Object obj:column) {
                        if ((Boolean) obj) {
                            returnString.append(RInstructions.TRUE + ",");
                        } else {
                            returnString.append(RInstructions.FALSE + ",");
                        }

                    }
                } else if (firstElement instanceof Integer) {
                    for (Object obj:column) {
                        returnString.append(
                                String.format(RInstructions.AS_INTEGER,obj));
                        returnString.append(",");
                    }
                } else {
                    for (Object obj:column) {
                        returnString.append(obj);
                        returnString.append(",");
                    }
                }

                returnString = new StringBuilder(returnString.substring(0,
                        returnString.length() - 1));
                returnString.append("),");
                
            }


        }
        if (!(this.rowNames.isEmpty())) {
            returnString.append("row.names=c(");
            for (String rowName : rowNames) {
                returnString.append("\"");
                returnString.append(rowName);
                returnString.append("\",");
            }
            returnString = new StringBuilder(returnString.substring(0, returnString.length() - 1));

            returnString.append("),stringsAsFactors=FALSE)");

        } else if (this.data.isEmpty()) {
            returnString.append(")");
        } else {
            returnString.append("stringsAsFactors=FALSE)");
        }

        if(log.isDebugEnabled()){
            log.debug(returnString);
        }

        return returnString.toString();
    }

    /**
     * Method to set the value (x,y) to a Double value.
     * 
     * @param x
     *            x coordinate (from 0 to size-1)
     * @param y
     *            y coordinate (from 0 to size-1)
     * @param data
     *            value to set
     * @throws RException
     *             if no variable name has been given to the data.frame or the
     * data type is not allowed at this location.
     */
    public void set(int x, int y, Double data) throws RException {
        checkVariable();
        checkX(x);
        checkY(y);
        try {
            if (this.data.get(x).get(y) instanceof Double){
                //set the value in the data.frame
                ((ArrayList<Double>) this.data.get(x)).set(y, data);

                //send the value to REngine
                engine.voidEval(String.format(RInstructions.SET_DATAFRAME_ITEM,
                        this.variable, y + 1, x + 1, data));
            } else {
                throw new RException(
                        "The data.frame does not accept this type on those coordinates : " +
                                data.getClass());
            }
        } catch (ClassCastException eee) {
            throw new RException(
                    "The data.frame does not accept this type on those coordinates : " +
                            data.getClass(), eee);
        }
    }

    public void set(int x, int y, Boolean data) throws RException {
        checkVariable();
        checkX(x);
        checkY(y);
        try {
            if (this.data.get(x).get(y) instanceof Boolean){
                //set the value in the data.frame
                ((ArrayList<Boolean>) this.data.get(x)).set(y, data);

                //send the value to REngine
                if (data) {
                    engine.voidEval(String.format(
                            RInstructions.SET_DATAFRAME_ITEM, this.variable,
                            y + 1, x + 1, RInstructions.TRUE));
                } else {
                    engine.voidEval(String.format(
                            RInstructions.SET_DATAFRAME_ITEM, this.variable,
                            y + 1, x + 1, RInstructions.FALSE));
                }
            } else {
                throw new RException(
                        "The data.frame does not accept this type on those coordinates : " +
                                data.getClass());
            }
        } catch (ClassCastException eee) {
            throw new RException(
                    "The data.frame does not accept this type on those coordinates : " +
                    data.getClass(), eee);
        }
    }

    public void set(int x, int y, String data) throws RException {
        checkVariable();
        checkX(x);
        checkY(y);
        try {
            if (this.data.get(x).get(y) instanceof String){
                //set the value in the data.frame
                ((ArrayList<String>) this.data.get(x)).set(y, data);

                //send the value to REngine
                engine.voidEval(String.format(RInstructions.SET_DATAFRAME_ITEM,
                        this.variable, y + 1, x + 1, "\"" + data + "\""));
            } else {
                throw new RException(
                        "The data.frame does not accept this type on those coordinates : " +
                                data.getClass());
            }
        } catch (ClassCastException eee) {
            throw new RException(
                    "The data.frame does not accept this type on those coordinates : " +
                            data.getClass(), eee);
        }
    }

    public void set(int x, int y, Integer data) throws RException {
        checkVariable();
        checkX(x);
        checkY(y);
        try {
            if (this.data.get(x).get(y) instanceof Integer){
                //set the value in the data.frame
                ((ArrayList<Integer>) this.data.get(x)).set(y, data);

                //send the value to REngine
                engine.voidEval(String.format(RInstructions.SET_DATAFRAME_ITEM,
                        this.variable, y + 1, x + 1, String.format(
                                RInstructions.AS_INTEGER, data)));
            } else {
                throw new RException(
                        "The data.frame does not accept this type on those coordinates : " +
                                data.getClass());
            }
        } catch (ClassCastException eee) {
            throw new RException(
                    "The data.frame does not accept this type on those coordinates : " +
                            data.getClass(), eee);
        }
    }

    /**
     * Method to get the object located at the [x,y] coordinates.
     * 
     * @param x
     *            x coordinates (between 0 and size-1)
     * @param y
     *            y coordinates (between 0 and size-1)
     * @return the Object located at the [x,y] coordinates
     * @throws RException
     *             if no variable name has been given to the data.frame
     * @throws IndexOutOfBoundsException
     *             if the x or y coordinates are not correct.
     */
    public Object get(int x, int y) throws RException {
        checkX(x);
        checkY(y);
        if (engine.isAutoCommit()) {
            checkVariable();
            Object returnObject = engine.eval(String.format(
                    RInstructions.GET_DATAFRAME_ITEM, this.variable, y + 1,
                    x + 1));
            
            ((ArrayList<Object>) this.data.get(x)).set(y,returnObject);
        }
        return this.data.get(x).get(y);
    }

    /**
     * Method to get the ArrayLists of the R data.frame. Synchronize with R only
     * if in autocommit mode and variable set.
     * 
     * @return the data of the data.frame
     */
    public List<List<?>> getData() {
        if (this.engine.isAutoCommit()){

            int rowNumber;
            int columnNumber;

            try {
                checkVariable();
                int[] dim = dim();
                rowNumber = dim[0];
                columnNumber = dim[1];
            } catch (RException eee) {
                return data;
            }

            initData(rowNumber, columnNumber);

            try {
                for(int i=0;i<columnNumber;i++){
                    for(int j=0;j<rowNumber;j++){
                        get(i,j);
                    }
                }

            } catch (RException eee) {
                log.error("An error occurred while trying to contact R");
            }

        }
        return data;
    }

    private void initData(int rowNumber, int columnNumber){


        this.data = new ArrayList<List<?>>();

        for (int i=0;i<columnNumber;i++){
            List<Object> column = new ArrayList<Object>();
            for(int j=0;j<rowNumber;j++){
                column.add(new Object());
            }
            this.data.add(column);
        }
    }

    /**
     * Method to assign the data of the R data.frame (there is no synchronizing
     * with R, use the commit() method to send data to R.
     * 
     * @param data
     *            a ArrayList of ArrayLists, containing each ArrayList of the R
     *            data.frame
     * @throws RException if an error occur while trying to assign the values
     */
    public void setData(List<List<?>> data) throws RException {
        this.data = data;
        engine.voidEval(toRString());
    }

    /**
     * Method to get a data.frame from a variable in R.
     * 
     * @param variable
     *            name of the data.frame in R
     * @throws RException
     */
    @Override
    public void getFrom(String variable) throws RException {
        this.variable = variable;
        if (engine.isAutoCommit()) {

            rowNames.clear();
            names.clear();
            data.clear();
            attributes.clear();

            //update data
            getData();

            //update row names
            getRowNames();

            //update names
            getNames();

            //update attributes
            getAttributes();
        }
    }

    /**
     * Export the data.frame into csv format.
     * 
     * @param outputFile the file to write
     * @param rowNames true/false to determine if the row names will be put on
     * the export.
     * @param names true/false to determine if the column names will be put on
     * the export.
     * @throws IOException if cannot export to the outputFile
     */
    public void exportCsv(File outputFile, boolean rowNames, boolean names)
            throws IOException {

        BufferedWriter file = null;
        try {
            file = new BufferedWriter(new FileWriter(outputFile));

            if (names) {
                if (rowNames) {
                    file.write(";");
                }
                for (String name : this.names) {
                    file.write(name + ";");
                }
                file.newLine();
            }

            for (int i = 0; i < this.data.get(0).size(); i++) {
                if (rowNames) {
                    file.write(this.rowNames.get(i) + ";");
                }
                for (List<?> aData : this.data) {
                    file.write(aData.get(i) + ";");
                }
                file.newLine();
            }
        } finally {
            if (file!=null){
                file.close();
            }
        }

    }

    /**
     * Import a dataframe form a csv file. The dataframe will contain Strings.
     * Use this method if you don't know the type of data that is in the csv
     * file.
     * 
     * @param inputFile
     *            Csv file to import.
     * @param rowNames
     *            Does the csv file contains names of the rows.
     * @param names
     *            Does the csv file contain names of the columns.
     * @throws IOException
     *            If cannot read the inputFile
     */
    public void importCsv(File inputFile, boolean rowNames, boolean names)
            throws IOException {
        List<Object> tmp = new ArrayList<Object>();
        tmp.add("string");
        importCsv(inputFile, rowNames, names, tmp);

    }

    /**
     * Import a dataframe form a csv file. The dataframe will contain Objects of
     * the same class than importType. Use this method if you know the type of
     * data that is in the csv file.
     * 
     * @param inputFile
     *            Csv file to import.
     * @param rowNames
     *            Does the csv file contains names of the rows.
     * @param names
     *            Does the csv file contain names of the columns.
     * @param importType
     *            Object of the class of the data imported (all the data have
     *            the same type). (Supported types : String, Double and Integer)
     * @throws IOException
     *            if cannot read the inputFile
     */
    public void importCsv(File inputFile, boolean rowNames, boolean names,
            Object importType) throws IOException {
        List<Object> tmp = new ArrayList<Object>();
        if (importType instanceof String) {
            tmp.add(importType);
            importCsv(inputFile, rowNames, names, tmp);
        } else if (importType instanceof Double) {
            tmp.add(importType);
            importCsv(inputFile, rowNames, names, tmp);
        } else if (importType instanceof Integer) {
            tmp.add(importType);
            importCsv(inputFile, rowNames, names, tmp);
        }
    }

    /**
     * Import a dataframe from a csv file. The dataframe will contain Objects of
     * the same class than each element of importTypes (one element = one
     * column). Use this method if you know the type of data that is in the csv
     * file.
     * 
     * @param inputFile
     *            Csv file to import.
     * @param rowNames
     *            Does the csv file contains names of the rows.
     * @param names
     *            Does the csv file contain names of the columns.
     * @param importTypes
     *            ArrayList of Object of the class of the data imported (the
     *            ArrayList match the data type of the columns). (Supported
     *            types : String, Double and Integer)
     * @throws IOException
     *            if cannot read the inputFile
     */
    public void importCsv(File inputFile, boolean rowNames, boolean names,
            List<Object> importTypes) throws IOException {

        //temporary String to read lines.
        String tmp;

        //temporary data list.
        List<List<?>> tempData = new ArrayList<List<?>>();

        Integer dataSize;
        
        BufferedReader br = null;
        try {
            //get the first line of the file
            br = new BufferedReader(new FileReader(inputFile));
            tmp = br.readLine();
            String[] splitted = tmp.split(";");

            //get the data size (number of columns)
            if (rowNames) {
                dataSize = splitted.length - 1;
            } else {
                dataSize = splitted.length;
            }

            //clear data, rowNames and names
            this.data.clear();
            this.rowNames.clear();
            this.names.clear();

            if (names) {
                //if names are present in the file, parse the first line to get
                //the names.
                this.names.addAll(Arrays.asList(splitted).subList(1, splitted.length));
            }

            //Initialize all the data columns with empty lists
            for (int i = 0; i < dataSize; i++) {
                List<Object> column = new ArrayList<Object>();
                tempData.add(column);
            }

            while ((tmp = br.readLine()) != null) {

                //parse each line
                splitted = tmp.split(";");

                //to determine the data index on the line.
                int index = 0;

                //if there are row names, extract the first item as the row name
                if (rowNames) {
                    this.rowNames.add(splitted[0]);
                    index = 1;
                }

                for (int i = index; i < splitted.length; i++) {
                    //cast the data imported to the specified type.
                    //test the size of the inputType list. If 1 import all the
                    //columns as the type of the first element.

                    //Get the column for index
                    ArrayList<Object> objects = (ArrayList<Object>)tempData.get(i - index);

                    //add to the column the converted value. If importTypes contains
                    //only one element, cast into this type, else cast into the
                    //column type.
                    if (((importTypes.size() == 1) &&
                            (importTypes.get(0) instanceof String) ) ||
                            ((importTypes.size() > (i -index )) &&
                                    (importTypes.get(i - index) instanceof String)) ) {

                        //We import strings so no cast in case of string
                        objects.add(splitted[i]);

                    } else if (((importTypes.size() == 1) &&
                            (importTypes.get(0) instanceof Double) ) ||
                            ((importTypes.size() > (i -index )) &&
                                    (importTypes.get(i - index) instanceof Double))) {

                        //in case of Double
                        objects.add(Double.valueOf(splitted[i]));

                    } else if (((importTypes.size() == 1) &&
                            (importTypes.get(0) instanceof Integer) ) ||
                            ((importTypes.size() > (i -index )) &&
                                    (importTypes.get(i - index) instanceof Integer))) {

                        //in case of Integer
                        objects.add(Integer.valueOf(splitted[i]));

                    }
                }
            }
        } finally {
            if (br != null){
                br.close();
            }
        }

        this.data = tempData;

        if (log.isDebugEnabled()){

            //Display the imported data.frame in debug mode
            log.debug("Imported DataFrame : \n"+toString());
        }

    }

    /**
     * Check if the index is into the data.frame length throws a
     * IndexOutOfBoundsException if the index is too big.
     *
     * @param y index
     */
    protected void checkY(int y) {
        if (!(this.data.get(0).isEmpty()) && (y > this.data.get(0).size())) {
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    y, this.data.size()));
        }
    }

    /**
     * Check if the index is into the data.frame length throws a
     * IndexOutOfBoundsException if the index is too big.
     *
     * @param x index
     */
    @Override
    public void checkX(int x) {
        if ((x > this.data.size()) && !(this.data.isEmpty())) {
            throw new IndexOutOfBoundsException(String.format(indexExceptionText,
                    x, this.data.size()));
        }
    }

    protected void checkType(Object o) throws RException {
        if (!(o instanceof String) && !(o instanceof Double) &&
                !(o instanceof Integer) && !(o instanceof Boolean)) {
            throw new RException("Not supported type");
        }
    }

    public int[] dim() throws RException {
        int x = (Integer)engine.eval("dim("+this.variable+")[1]");
        int y = (Integer)engine.eval("dim("+this.variable+")[2]");

        return (new int[]{x,y});
    }

    /**
     * Method that returns a string representing the DataFrame. The output
     * String should looks like the dataframe in R.
     * @return a string representing the dataframe as in R.
     */
    @Override
    public String toString(){

        List<String> linesToDisplay = new ArrayList<String>();

        Boolean displayRowNames = !rowNames.isEmpty();

        Boolean displayNames = !names.isEmpty();

        Integer numberOfLines = 0;

        //Calculate the number of lines to display
        if ((!this.data.isEmpty()) &&(!this.data.get(0).isEmpty())){
                numberOfLines = this.data.get(0).size();
        }
        if (displayNames){
            numberOfLines += 1;
        }

        // fill the lines with empty strings
        for (int i=0;i<numberOfLines;i++){
            linesToDisplay.add(i,"");
        }

        // Add the row names if necessary
        if (displayRowNames){

            // calculate the column size
            int columnSize = 0;
            for (String name:rowNames){
                if (name.length()>columnSize){
                    columnSize = name.length();
                }
            }
            columnSize +=2;

            // add the row name to each line, complete with spaces to have next
            // column aligned
            if (displayNames) {
                String line = linesToDisplay.get(0);
                String str = String.format("%-"+columnSize+"s", "");
                line +=str ;
                    linesToDisplay.set(0, line);
            }
            for (int i=0;i<rowNames.size();i++){
                String str = String.format("%-"+columnSize+"s", rowNames.get(i));

                if(displayNames) {
                    String line = linesToDisplay.get(i+1);
                    line += str;
                    linesToDisplay.set(i+1, line);
                } else {
                    String line = linesToDisplay.get(i);
                    line += str;
                    linesToDisplay.set(i, line);
                }

            }

        }

        // for each column, calculate the column size, add the value to each
        // line, complete with spaces to have next column aligned
        for (List<?> column:this.data){

            String columnName="";

            // calculate the column size
            int columnSize = 0;
            if(displayNames){
                columnName = names.get(this.data.indexOf(column));
                columnSize = columnName.length();
            }
            for (Object obj:column){
                String toString = obj.toString();
                if (toString.length()>columnSize){
                    columnSize = toString.length();
                }
            }
            columnSize +=2;

            if(displayNames){
                String str = String.format("%-"+columnSize+"s", columnName);
                String line = linesToDisplay.get(0);
                line += str;
                linesToDisplay.set(0, line);
            }

            for (int i=0;i<column.size();i++){

                String str = String.format("%-"+columnSize+"s", column.get(i));

                if(displayNames) {
                    String line = linesToDisplay.get(i+1);
                    line += str;
                    linesToDisplay.set(i+1, line);
                } else {
                    String line = linesToDisplay.get(i);
                    line += str;
                    linesToDisplay.set(i, line);
                }
            }
        }

        StringBuffer returnString = new StringBuffer();

        for (String str:linesToDisplay){
            returnString.append(str);
            returnString.append("\n");
        }

        return returnString.toString();
    }
}
