/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r;

import java.io.File;

/**
 * Abstract REngine that provides utility R methods (such as clear session, set
 * working directory, import data, ...) for all R engines.
 * 
 * @author couteau
 */
public abstract class REngineAbstract implements REngine {

    /**
     * If true, commit each R instruction on the fly, if false, commit only when
     * the commit() method is called.
     */
    protected Boolean autocommit = true;

    /**
     * Load a specific R session :
     * Load .RData file located in directory
     * 
     * @param directory
     *            directory where the .RData file is located
     * @throws RException if an error occur while loading the R session file
     */
    @Override
    public void loadRData(File directory) throws RException {
        setwd(directory);
        voidEval(RInstructions.LOAD_RDATA);
    }

    /**
     * Load a specific R session :
     * Load the "fileName.RData" file located in directory.
     *
     * @param directory directory where the ".RData" file is located
     * @param fileName name of the file to load (will load the fileName.RData
     * file)
     * @throws org.nuiton.j2r.RException if an error occur while loading the R
     * session file.
     */
    @Override
    public void loadRData(File directory, String fileName) throws RException {
        setwd(directory);
        voidEval(String.format(RInstructions.LOAD_RDATA_FILE, fileName));
    }

    /**
     * Load the "fileName.RData" file located in current working directory.
     *
     * @param fileName name of the file to load (will load the fileName.RData
     * file)
     * @throws org.nuiton.j2r.RException if an error occur while loading the R
     * session file.
     */
    @Override
    public void loadRData(String fileName) throws RException {
        voidEval(String.format(RInstructions.LOAD_RDATA_FILE, fileName));
    }

    /**
     * Save the session in a .RData file in directory
     * 
     * @param directory
     *            where the .RData file will be saved
     * @throws RException if an error occur while saving the R
     * session.
     */
    @Override
    public void saveRData(File directory) throws RException {
        setwd(directory);
        voidEval(RInstructions.SAVE_IMAGE);
    }

    /**
     * Save the session in a .RData file in current working directory
     *
     * @param filename
     *            the .RData filename (will be followed by .RData)
     * @throws RException if an error occur while saving the R
     * session.
     */
    @Override
    public void saveRData(String filename) throws RException {
        voidEval(String.format(RInstructions.SAVE_IMAGE_FILE, filename));
    }

    /**
     * Save a R session in a fileName.RData file located in directory.
     *
     * @param directory where the ".RData" file will be located
     * @param fileName name of the file to save (will save in the fileName.RData
     * file)
     * @throws org.nuiton.j2r.RException if an error occur while saving the R
     * session.
     */
    @Override
    public void saveRData(File directory, String fileName) throws RException {
        setwd(directory);
        voidEval(String.format(RInstructions.SAVE_IMAGE_FILE, fileName));
    }

    /**
     * Set the R working directory
     * 
     * @param directory
     *            to set
     * @throws RException
     */
    @Override
    public void setwd(File directory) throws RException {
        voidEval(String.format(RInstructions.SET_WORKING_DIRECTORY,
                directory.getAbsolutePath().replaceAll("\\\\", "/")));
    }

    /**
     * Get the actual R session working directory
     * 
     * @return a File that is the actual R session working directory
     * @throws RException
     */
    @Override
    public File getwd() throws RException {
        String directory = (String) eval(RInstructions.GET_WORKING_DIRECTORY);
        return new File(directory);
    }

    /**
     * Use the dput R instruction to store the content of a R object to a file.
     * The file created will be in the working directory
     * 
     * @param rObject
     *            name of the R object to save
     * @param outputFileName
     *            name of the file to save
     * @throws RException
     */
    @Override
    public void dput(String rObject, String outputFileName) throws RException {
        voidEval(String.format(RInstructions.DPUT, rObject, outputFileName));
    }

    /**
     * Use the dget rInstruction to store the content of a file (created with
     * the dput instruction) into a R object. The file used have to be in the
     * working directory
     * 
     * @param rObject
     *            name of the R object created
     * @param inputFileName
     *            name of the file to load
     * @throws RException
     */
    @Override
    public void dget(String rObject, String inputFileName) throws RException {
        voidEval(String.format(RInstructions.DGET, rObject, inputFileName));

    }

    /**
     * Use the dput R instruction to store the content of a R object to a file.
     * 
     * @param rObject
     *            R object to save
     * @param outputFile
     *            the file to save
     * @throws RException
     */
    @Override
    public void dput(String rObject, File outputFile) throws RException {
        //Store the previous working directory
        File workingdir = getwd();
        //Set the destination working directory
        setwd(outputFile.getParentFile());
        //Save the R object.
        voidEval(String.format(RInstructions.DPUT, rObject, outputFile.getName()));
        //Go back to previous working directory
        setwd(workingdir);
    }

    /**
     * Use the dget rInstruction to store the content of a file (created with
     * the dput instruction) into a R object.
     * 
     * @param rObject
     *            name of the R object created
     * @param inputFile
     *            file to load
     * @throws RException
     */
    @Override
    public void dget(String rObject, File inputFile) throws RException {
        //Store the previous working directory
        File workingdir = getwd();
        //Set the source working directory
        setwd(inputFile.getParentFile());
        //Load the inputFile
        voidEval(String.format(RInstructions.DGET, rObject, inputFile.getName()));
        //Go back to the previous working directory
        setwd(workingdir);
    }

    /**
     * Remove a R object from the R session using the R method : remove(rObject).
     *
     * @param rObject name of the R object to be removed.
     * @throws org.nuiton.j2r.RException if an error occur while trying to
     * remove the R object.
     */
    @Override
    public void remove(String rObject) throws RException {
        voidEval(String.format(RInstructions.REMOVE, rObject));
    }

    /**
     * Method similar to  the linux mv instruction : move an object to a
     * destination, or in this case, rename a R object.
     * Does :
     * destination&lt;-source
     * remove(source)
     *
     * @param source source R object name
     * @param destination destination R object name
     * @throws org.nuiton.j2r.RException if an error occur while trying to move
     * the R object.
     */
    @Override
    public void mv(String source, String destination) throws RException {
        cp(source, destination);
        remove(source);

    }

    /**
     * Copy a R Object into another one (similar to the linux cp instruction).
     * Does :
     * destination&lt;-source
     *
     * @param source source R object name
     * @param destination destination R object name
     * @throws org.nuiton.j2r.RException if an error occur while trying to
     * assign the source to the destination
     */
    @Override
    public void cp(String source, String destination) throws RException {
        voidEval(destination + "<-" + source);

    }

    /**
     * Method to get all the R object in session. Return the result of the R ls()
     * function.
     *
     * @return the names of the R objects present in session as a String[]. The
     * order of the names is given by their order given by the R ls() method,
     * e.g. alphabetic order.
     * @throws org.nuiton.j2r.RException if an error occur while trying to list
     * the R objects in session.
     */
    @Override
    public String[] ls() throws RException {
        String[] rObjects = (String[]) eval(RInstructions.LS);
        return rObjects;
    }

    /**
     * Method to clear the R session. Does : rm(list=ls())
     *
     * @throws org.nuiton.j2r.RException if an error occur while trying to clear
     * the session
     */
    @Override
    public void clearSession() throws RException {
        voidEval(RInstructions.CLEAR_SESSION);
    }

    /**
     * Change the autocommit mode of the engine. If switching from no-autocommit
     * to autocommit mode, does a commit of all R instructions stored.
     *
     * @param autocommit true for autocommit mode, false for no-autocommit mode.
     * @throws org.nuiton.j2r.RException if an error occur while commiting all
     * the R instructions stored.
     */
    @Override
    public void setAutoCommit(Boolean autocommit) throws RException {
        if ((!this.autocommit) && (autocommit)) {
            commit();
        }
        this.autocommit = autocommit;
    }

    /**
     * Method to know if engine is in autocommit mode.
     *
     * @return true if in autocommit mode, false otherwise.
     */
    @Override
    public Boolean isAutoCommit() {
        return this.autocommit;
    }

    /**
     * @param filename
     * @param x
     * @param y
     * @param type
     * @param main
     * @param sub
     * @param xlab
     * @param ylab
     * @param asp
     */
    public void plot(String filename, String x, String y, String type,
                     String main, String sub, String xlab, String ylab,
                     String asp) throws RException {

        eval("jpeg(\"" + filename + ".jpg\")");

        StringBuilder rinstruction = new StringBuilder("plot(");

        if ((x != null) && (!x.isEmpty())) {
            rinstruction.append(x);
        } else {
            throw new IllegalArgumentException("x argument must be filled");
        }

        if ((y != null) && (!y.isEmpty())) {
            rinstruction.append(",y=");
            rinstruction.append(y);
        }

        if (type != null) {
            rinstruction.append(",type=\"");
            rinstruction.append(type);
            rinstruction.append("\"");
        }

        if (main != null) {
            rinstruction.append(",main=\"");
            rinstruction.append(main);
            rinstruction.append("\"");
        }

        if (sub != null) {
            rinstruction.append(",sub=\"");
            rinstruction.append(sub);
            rinstruction.append("\"");
        }

        if (xlab != null) {
            rinstruction.append(",xlab=\"");
            rinstruction.append(xlab);
            rinstruction.append("\"");
        }

        if (ylab != null) {
            rinstruction.append(",ylab=\"");
            rinstruction.append(ylab);
            rinstruction.append("\"");
        }

        if (asp != null) {
            rinstruction.append(",asp=");
            rinstruction.append(asp);
            rinstruction.append("\"");
        }

        rinstruction.append(")");

        eval(rinstruction.toString());
        eval("dev.off()");

    }
}
