/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
 * RException.java
 *
 * Created: 22 aout 06
 *
 * @author Arnaud Thimel <thimel@codelutin.com>
 */
package org.nuiton.j2r;

/**
 * Exception thrown when an error occur during the communication with R
 * (whatever the communication mode is).
 */
public class RException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Exception constructor.
     */
    public RException() {
    }

    /**
     * Exception constructor
     * @param message text describing the exception.
     */
    public RException(String message) {
        super(message);
    }

    /**
     * Exception constructor.
     * @param cause the Throwable instance that throwed this exception.
     */
    public RException(Throwable cause) {
        super(cause);
    }

    /**
     * Exception constructor.
     * @param message text describing the exception.
     * @param cause the Throwable instance that throwed this exception.
     */
    public RException(String message, Throwable cause) {
        super(message, cause);
    }
} //RException
