/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
 * RProxy.java
 *
 * Created: 21 aout 2006
 *
 * @author Arnaud Thimel <thimel@codelutin.com>
 */
package org.nuiton.j2r;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.jni.RJniEngine;
import org.nuiton.j2r.net.RNetEngine;

/**
 * This class allows to initialize a REngine without knowing which
 * implementation is chosen. The implementation detection is based on the
 * JVM's options :
 * <ul>
 * <li><code>-DR.type=net</code></li>
 * <li><code>-DR.type=jni</code></li>
 * <li><code>-DR.type=...</code></li>
 * </ul>
 * It is possible to parametrize the network solution, for that, see the
 * {@link RNetEngine} documentation.
 */
public class RProxy implements REngine {

    private Log log = LogFactory.getLog(RProxy.class);
    private REngine engine;

    /**
     * Constructor. Launches the initialization.
     *
     * @throws RException if initialisation failed
     */
    public RProxy() throws RException {
        Boolean init = init();
        if (!init) {
            throw new RException(
                "R initialisation failed, please check your installation");
        }
    }

    /**
     * Initialization method with specified R engine type.
     *
     * @param rType the R engine type (net or jni)
     *
     * @return true if successfully initialized, false otherwise.
     */
    private boolean init(String rType) {
        boolean initSucceded = false;
        if (rType == null) {
            rType = "net";
        }
        if (rType.startsWith("net")) {
            //tries a net initialization
            initSucceded = initOnNet();
            if (!initSucceded) {
                //if no success on net, tries on JNI
                if (log.isErrorEnabled()) {
                    log.error(
                        "Initialization of R with Network failed, trying JNI");
                }
                initSucceded = initOnJNI();
            }
        } else if ("jni".equalsIgnoreCase(rType)) {
            //tries a jni initialization
            initSucceded = initOnJNI();
            if (!initSucceded) {
                //if no success on jni, tries on net
                if (log.isErrorEnabled()) {
                    log.error(
                        "Initialization of R over JNI failed, trying with Network");
                }
                initSucceded = initOnNet();
            }
        } else if (log.isErrorEnabled()) {
            log.error("Invalid type specified : " + rType);
        }
        return initSucceded;
    }

    /**
     * Method to initialize REngine on JNI
     *
     * @return true is succesfully initialized, false otherwise.
     */
    private boolean initOnJNI() {
        if (log.isInfoEnabled()) {
            log.info("Trying to initialize the R Proxy over JNI");
        }
        RJniEngine newEngine = new RJniEngine();
        if (newEngine.init()) {
            engine = newEngine;
            return true;
        }
        return false;
    }

    /**
     * Method to initialize REngine on net
     *
     * @return true is succesfully initialized, false otherwise.
     */
    private boolean initOnNet() {
        if (log.isInfoEnabled()) {
            log.info("Trying to initialize the R Proxy with Network");
        }
        RNetEngine newEngine = new RNetEngine();
        if (newEngine.init()) {
            engine = newEngine;
            return true;
        }
        return false;
    }

    /**
     * Send a R instruction to the engine and get the result back.
     *
     * @param expr the R instruction to send.
     *
     * @return the result of the R instruction.
     *
     * @see REngine#eval(String)
     */
    @Override
    public Object eval(String expr) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return null;
        }
        return engine.eval(expr);
    }

    /**
     * Send a R multi-line instruction to the engine and get the result back.
     *
     * @param expr the R instruction to send.
     * @return the result of the R instruction.
     * @see REngine#eval(String)
     */
    @Override
    public Object evalScript(String expr) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                    "occured during the initialization.");
            return null;
        }
        return engine.evalScript(expr);
    }

    /**
     * Get back the R engine type options to initialize the REngine.
     *
     * @see REngine#init()
     */
    @Override
    public boolean init() {
        String rType = System.getProperty("R.type");
        if (rType == null || "".equals(rType)) {
            log.warn("No R type specified.\nusage:\n\tjava -DR.type=net ...\n" +
                "or\n\tjava -DR.type=jni ...\n\nConsidering network type");
            rType = "net";
        }
        if (!init(rType)) {
            if (log.isFatalEnabled()) {
                log.fatal("The initializing of R failed.");
            }
            return false;
        }
        return true;
    }

    /**
     * Terminate the R engine.
     *
     * @see REngine#terminate()
     */
    @Override
    public void terminate() throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
        } else {
            engine.terminate();
        }
    }

    /**
     * Send a R instruction to the engine without getting back its result.
     *
     * @param expr the R instruction to send.
     *
     * @see REngine#voidEval(String)
     */
    @Override
    public void voidEval(String expr) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.voidEval(expr);
    }

    /**
     * Load .RData file located in directory
     * 
     * @param directory
     *            directory where the .RData file is located
     * @throws RException if an error occcur while loading the R session.
     *
     * @see REngine#loadRData(File)
     * @see REngineAbstract#loadRData(File)
     */
    @Override
    public void loadRData(File directory) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.setwd(directory);
        engine.voidEval("load(\".RData\")");
    }

    /**
     * Save the session in a .RData file in directory
     * 
     * @param directory
     *            where the .RData file will be saved
     * @throws RException if an error occur while saving the R session.
     *
     * @see REngine#saveRData(File)
     * @see REngineAbstract#saveRData(File)
     */
    @Override
    public void saveRData(File directory) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.setwd(directory);
        engine.voidEval("save.image()");
    }

    /**
     * Set the R working directory
     * 
     * @param directory the new working directory to set.
     *
     * @throws RException if an error occur while setting the R working directory.
     *
     * @see REngine#setwd(File)
     * @see REngineAbstract#setwd(File)
     */
    @Override
    public void setwd(File directory) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.setwd(directory);
    }

    /**
     * Get the actual R session working directory
     * 
     * @return a File that is the actual R session working directory, null if
     *         the engine is not initialized.
     *
     * @throws RException if an error occur while getting the R working directory.
     *
     * @see REngine#getwd()
     * @see REngineAbstract#getwd()
     */
    @Override
    public File getwd() throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return null;
        }
        return engine.getwd();
    }

    /**
     * Use the dput R instruction to store the content of a R object to a file.
     * The file created will be in the working directory
     * 
     * @param rObject
     *            name of the R object to save
     * @param outputFileName
     *            name of the file to save
     * @throws RException if an error occur while saving the content of the
     * object.
     *
     * @see REngine#dput(String, String)
     * @see REngineAbstract#dput(String, String)
     */
    @Override
    public void dput(String rObject, String outputFileName) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        String rInstruction = "dput(%s,file=\"%s\")";
        engine.voidEval(String.format(rInstruction, rObject, outputFileName));
    }

    /**
     * Use the dget R instruction to store the content of a file (created with
     * the dput R instruction) into a R object. The file used have to be in the
     * working directory
     * 
     * @param rObject
     *            name of the R object created
     * @param inputFileName
     *            name of the file to load
     * @throws RException if an error occur while trying to perform this
     * operation.
     *
     * @see REngine#dget(String, String)
     * @see REngineAbstract#dget(String, String)
     */
    @Override
    public void dget(String rObject, String inputFileName) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        String rInstruction = "%s <- dget(\"%s\")";
        engine.voidEval(String.format(rInstruction, rObject, inputFileName));

    }

    /**
     * Use the dput R instruction to store the content of a R object to a file.
     * 
     * @param rObject
     *            R object to save
     * @param outputFile
     *            the file to save
     * @throws RException if an error occur while performing the operation.
     *
     * @see REngine#dput(String, File)
     * @see REngineAbstract#dput(String, File)
     */
    @Override
    public void dput(String rObject, File outputFile) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        File workingdir = getwd();
        engine.setwd(outputFile.getParentFile());
        String rInstruction = "dput(%s,file=\"%s\")";
        engine.voidEval(String.format(rInstruction, rObject,
            outputFile.getName()));
        setwd(workingdir);
    }

    /**
     * Use the dget R instruction to store the content of a file (created with
     * the dput instruction) into a R object.
     * 
     * @param rObject
     *            name of the R object created
     * @param inputFile
     *            file to load
     * @throws RException if an eroor occur while performing the operation.
     *
     * @see REngine#dget(String, File)
     * @see REngineAbstract#dget(String, File)
     */
    @Override
    public void dget(String rObject, File inputFile) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        File workingdir = getwd();
        engine.setwd(inputFile.getParentFile());
        String rInstruction = "%s <- dget(\"%s\")";
        engine.voidEval(
            String.format(rInstruction, rObject, inputFile.getName()));
        setwd(workingdir);
    }

    /**
     * Remove an object from the R session using the remove(rObject) R function.
     *
     * @param rObject name of the R object to remove.
     *
     * @throws RException if an error occur while removing the R
     * object.
     *
     * @see REngine#remove(String)
     * @see REngineAbstract#remove(String)
     */
    @Override
    public void remove(String rObject) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.remove(rObject);
    }

    /**
     * Method similar to the linux mv command : rename a R object.
     *
     * @param source the source R object name
     *
     * @param destination the destination R object name
     *
     * @throws RException if an error occur while renaming the R
     * object.
     *
     * @see REngine#mv(String, String)
     * @see REngineAbstract#mv(String, String)
     */
    @Override
    public void mv(String source, String destination) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.mv(source, destination);

    }

    /**
     * Method similar to the linux cp command : copy a R object.
     *
     * @param inputObject the source R object naame
     *
     * @param outputObject the destination R object name
     *
     * @throws RException if an error occur while copying the R
     * object.
     *
     * @see REngine#cp(String, String)
     * @see REngineAbstract#cp(String, String)
     */
    @Override
    public void cp(String inputObject, String outputObject) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.cp(inputObject, outputObject);

    }

    /**
     * Method to get all the R objects present in session.
     *
     * @return a table containing all the R objects present in session.
     *
     * @throws RException if an error occur while getting back
     * the objects list.
     *
     * @see REngine#ls()
     * @see REngineAbstract#ls()
     */
    @Override
    public String[] ls() throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return null;
        }
        return engine.ls();
    }

    /**
     * Method to remove all objects from the R session.
     * 
     * @throws RException if an error occur while clearing the R
     * session.
     *
     * @see REngine#clearSession()
     * @see REngineAbstract#clearSession()
     */
    @Override
    public void clearSession() throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.clearSession();
    }

    /**
     * Method to launch the evaluation of all the R expressions stored when in
     * non-autocommit mode.
     *
     * @throws RException if an error occur while evaluating one
     * of the R expressions.
     *
     * @see REngine#commit()
     * @see REngineAbstract#commit()
     */
    @Override
    public void commit() throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.commit();
    }

    /**
     * Method to set the autocommit mode :
     * <ul><li>true : autocommit mode</li>
     * <li>false : non-autocommit mode</li></ul>
     *
     * @param autocommit the autocommit mode
     *
     * @throws RException if an error occur while switching
     * modes.
     *
     * @see REngine#setAutoCommit(Boolean)
     * @see REngine#setAutoCommit(Boolean)
     */
    @Override
    public void setAutoCommit(Boolean autocommit) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return;
        }
        engine.setAutoCommit(autocommit);
    }

    /**
     * Method to know the autocommit mode :
     * <ul><li>true : autocommit mode</li>
     * <li>false : non-autocommit mode</li></ul>
     *
     * @return the autocommit mode
     *
     * @see REngine#isAutoCommit()
     * @see REngineAbstract#isAutoCommit()
     */
    @Override
    public Boolean isAutoCommit() {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
            return null;
        }
        return engine.isAutoCommit();
    }

    /**
     * Save a R session in a fileName.RData file located in directory.
     *
     * @param directory where the ".RData" file will be located
     *
     * @param fileName name of the file to save (will save in the fileName.RData
     * file)
     *
     * @throws RException if an error occur while saving the R
     * session.
     *
     * @see REngine#saveRData(File, String)
     * @see REngineAbstract#saveRData(File, String)
     */
    @Override
    public void saveRData(File directory, String fileName) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
        }
        engine.saveRData(directory, fileName);
    }

    /**
     * Load a specific R session :
     * Load the "fileName.RData" file located in directory.
     *
     * @param directory directory where the ".RData" file is located
     *
     * @param fileName name of the file to load (will load the fileName.RData
     * file)
     *
     * @throws RException if an error occur while loading the R
     * session file.
     *
     * @see REngine#loadRData(File, String)
     * @see REngineAbstract#loadRData(File, String)
     */
    @Override
    public void loadRData(File directory, String fileName) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
        }
        engine.loadRData(directory, fileName);
    }

    @Override
    public void saveRData(String filename) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
        }
        engine.saveRData(filename);
    }

    @Override
    public void loadRData(String filename) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                "occured during the initialization.");
        }
        engine.loadRData(filename);
    }

    @Override
    public void plot(String filename, String x, String y, String type, String main, String sub, String xlab, String ylab, String asp) throws RException {
        if (engine == null) {
            log.fatal("The R Proxy is not initialized. An error probably " +
                    "occured during the initialization.");
        }
        engine.plot(filename, x, y, type, main, sub, xlab, ylab, asp);
    }

    /**
     * To know if the engine interfaced is a RNetEngine.
     *
     * @return true if the engine interfaced is a RNetEngine.
     */
    public boolean isNet() {
        if (engine != null) {
            return engine instanceof RNetEngine;
        } else {
            return false;
        }
    }

    /**
     * To know if the engine interfaced is a RJniEngine.
     *
     * @return true if the engine interfaced is a RJniEngine.
     */
    public boolean isJni() {
        if (engine != null) {
            return engine instanceof RJniEngine;
        } else {
            return false;
        }
    }

} //RProxy

