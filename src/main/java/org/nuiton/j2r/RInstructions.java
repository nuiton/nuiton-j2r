/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r;

/**
 *
 * @author couteau
 */
public interface RInstructions {

    /**
     * Load the .RData file located in the working directory
     */
    String LOAD_RDATA = "load(\".RData\")";
    /**
     * Load the file.RData file located in the working directory
     */
    String LOAD_RDATA_FILE = "load(\"%s.RData\")";
    String SAVE_IMAGE = "save.image()";
    String SAVE_IMAGE_FILE = "save.image(file=\"%s.RData\")";
    String SET_WORKING_DIRECTORY = "setwd(\"%s\")";
    String GET_WORKING_DIRECTORY = "getwd()";
    String DPUT = "dput(%s,file=\"%s\")";
    String DGET = "%s <- dget(\"%s\")";
    String REMOVE = "remove(%s)";
    String LS = "ls()";
    String CLEAR_SESSION = "rm(list=ls())";
    String RTRY = "try(%s,silent=TRUE)";
    String CLASS_ERROR = "try-error";
    String CLASS_DATAFRAME = "data.frame";
    String ATTRIBUTE_CLASS = "class";
    String ATTRIBUTE_NAMES = "names";
    String ATTRIBUTE_ROWNAMES = "row.names";
    String GET_ROW_NAMES = "row.names(%s)";
    String GET_ROW_NAME = "row.names(%s)[%s]";
    String SET_ROW_NAMES = "row.names(%s)<-c(%s)";
    String SET_ROW_NAME = "row.names(%s)[%s]<-\"%s\"";
    String GET_NAMES = "names(%s)";
    String GET_NAME = "names(%s)[%s]";
    String SET_NAMES = "names(%s)<-c(%s)";
    String SET_NAME = "names(%s)[%s]<-\"%s\"";
    String SET_ATTRIBUTE = "attr(%s,\"%s\")<-%s";
    String GET_ATTRIBUTE = "attr(%s,\"%s\")";
    String LENGTH = "length(%s)";
    String LENGTH_COLUMN = "length(%s[,%s])";
    String LENGTH_ATTRIBUTES = "length(attributes(%s))";
    String GET_ATTRIBUTE_NAME = "names(attributes(%s))[%s]";
    String GET_LIST_ITEM = "%s[[%s]]";
    String GET_DATAFRAME_ITEM ="%s[%s,%s]";
    String SET_DATAFRAME_ITEM ="%s[%s,%s]<-%s";
    String SET_LIST_ITEM = "%s[[%s]]<-%s";
    String TRUE = "TRUE";
    String FALSE = "FALSE";
    String AS_INTEGER="as.integer(%s)";
}
