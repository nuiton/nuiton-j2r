/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
 * RInterface.java
 *
 * Created: 21 aout 2006
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
package org.nuiton.j2r;

import java.io.File;

/**
 * This interface is the common part between the different technologies used to
 * access R.
 */
public interface REngine {

    /**
     * Initialize the engine. The parameters are given with the JVM options :
     * -D...
     * 
     * @return true/false to indicate if the initialization was successful.
     */
    boolean init();

    /**
     * Evaluate the expression given in parameter in R.
     * 
     * @param expr the expression to evaluate.
     *
     * @return the value return by the expression.
     *
     * @throws RException if an error occur while trying to evaluate the
     * expression.
     */
    Object eval(String expr) throws RException;

    /**
     * Evaluate the expression given in parameter in R without result returned.
     * When it is possible, it allows to save time (transfer and conversion from
     * R to Java).
     * 
     * @param expr the expression to evaluate
     *
     * @throws RException if an error occured while trying to evaluate the
     * expression.
     */
    void voidEval(String expr) throws RException;

    /**
     * Evaluate the multi-line expression given in parameter in R.
     *
     * @param expr the multi-line expression to evaluate.
     * @return the value returned by the expression.
     * @throws RException if an error occur while trying to evaluate the
     *                    expression.
     */
    Object evalScript(String expr) throws RException;

    /**
     * End the engine use.
     * 
     * @throws RException
     */
    void terminate() throws RException;

    /**
     * Load .RData file located in directory
     * 
     * @param directory
     *            directory where the .RData file is located
     * @throws RException
     */
    void loadRData(File directory) throws RException;

    /**
     * Load filename.RData file located in current working directory
     *
     * @param filename
     *            name of the .RData file (will be followed by .RData)
     * @throws RException
     */
    void loadRData(String filename) throws RException;

    /**
     * Save the session in a .RData file in directory
     * 
     * @param directory
     *            where the .RData file will be saved
     * @throws RException
     */
    void saveRData(File directory) throws RException;

    /**
     * Set the R working directory
     * 
     * @param directory
     *            to set
     * @throws RException
     */
    void setwd(File directory) throws RException;

    /**
     * Get the actual R session working directory
     * 
     * @return a File that is the actual R session working directory
     * @throws RException
     */
    File getwd() throws RException;

    /**
     * Use the dput R instruction to store the content of a R object to a file.
     * The file created will be in the working directory
     * 
     * @param rObject
     *            name of the R object to save
     * @param outputFileName
     *            name of the file to save
     * @throws RException
     */
    void dput(String rObject, String outputFileName) throws RException;

    /**
     * Use the dget rInstruction to store the content of a file (created with
     * the dput instruction) into a R object. The file used have to be in the
     * working directory
     * 
     * @param rObject
     *            name of the R object created
     * @param inputFileName
     *            name of the file to load
     * @throws RException
     */
    void dget(String rObject, String inputFileName) throws RException;

    /**
     * Use the dput R instruction to store the content of a R object to a file.
     * 
     * @param rObject
     *            R object to save
     * @param outputFile
     *            the file to save
     * @throws RException
     */
    void dput(String rObject, File outputFile) throws RException;

    /**
     * Use the dget rInstruction to store the content of a file (created with
     * the dput instruction) into a R object.
     * 
     * @param rObject
     *            name of the R object created
     * @param inputFile
     *            file to load
     * @throws RException
     */
    void dget(String rObject, File inputFile) throws RException;

    /**
     * Remove a R object from the actual session
     * 
     * @param rObject
     *            to be removed from the session
     * @throws RException
     */
    void remove(String rObject) throws RException;

    /**
     * Make a copy of an object and remove the old one.
     * 
     * @param source
     *            the object to be copied and deleted
     * @param destination
     *            the object to be created
     * @throws RException
     */
    void mv(String source, String destination) throws RException;

    /**
     * Copy an object.
     * 
     * @param source
     *            the object to be copied
     * @param destination
     *            the object to be created
     * @throws RException
     */
    void cp(String source, String destination) throws RException;

    /**
     * List all R object present in the actual session
     * 
     * @return a list containing the name of all the R objects in the R session.
     * @throws RException
     */
    String[] ls() throws RException;

    /**
     * Remove all the objects present in the actual R session.
     * 
     * @throws RException
     */
    void clearSession() throws RException;

    /**
     * Set the autocommit property. When switching from autocommit = false to
     * true, the commit operation is performed.
     * 
     * @param autocommit
     *            If true, R instructions are sent when they are thrown. If
     *            false, the instructions are stored in a LinkedList and the
     *            user needs to explicit the commit() instruction.
     * @throws RException
     *             if an error occured during the commit operation.
     */
    void setAutoCommit(Boolean autocommit) throws RException;

    /**
     * Return the autocommit status
     * 
     * @return true if engine in auto-commit mode
     */
    Boolean isAutoCommit();

    /**
     * Commit all the R instructions that have been stored and not commited yet.
     * @throws RException
     */
    void commit() throws RException;

    /**
     * Load a specific R session :
     * Load the "fileName.RData" file located in directory.
     *
     * @param directory directory where the ".RData" file is located
     *
     * @param fileName name of the file to load (will load the fileName.RData
     * file)
     *
     * @throws org.nuiton.j2r.RException if an error occur while loading the R
     * session file.
     */
    void loadRData(File directory, String fileName) throws RException;

    /**
     * Save a R session in a fileName.RData file located in directory.
     *
     * @param directory where the ".RData" file will be located
     *
     * @param fileName name of the file to save (will save in the fileName.RData
     * file)
     * 
     * @throws org.nuiton.j2r.RException if an error occur while saving the R
     * session.
     */
    void saveRData(File directory, String fileName) throws RException;

    /**
     * Save a R session in a filename.RData file located in the current working
     * directory
     * 
     * @param filename
     * @throws RException
     */
    void saveRData(String filename) throws RException;

    void plot(String filename, String x, String y, String type,
                     String main, String sub, String xlab, String ylab,
                     String asp) throws RException;
} //REngine
