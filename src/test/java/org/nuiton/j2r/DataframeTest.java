/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.j2r.types.RDataFrame;

public class DataframeTest {

    private static Log log = LogFactory.getLog(DataframeTest.class);

    private REngine engine;

    @BeforeClass
    public static void tryREnv(){
        try {
            new RProxy();
        } catch (RException eee) {
            if (log.isErrorEnabled()) {
                log.error("No R environment found to run tests. Skip");
            }
            Assume.assumeTrue(false);
        }
    }

    @Before
    public void setUp() throws Exception {
        engine = new RProxy();
    }

    @After
    public void tearDown() throws Exception {
        if (engine != null){
            engine.terminate();
        }
    }

    @Test
    public void testDataFrameCreationMethod1() throws Exception {
        /*
         * This test data.frame creation method 1 and data assignment : 
         * RDataFrame(REngine) + all setters
         */

        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<String> column2 = new ArrayList<String>();
        column2.add("titi");
        column2.add("tata");
        column2.add("toto");

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<Integer> column4 = new ArrayList<Integer>();
        column4.add(1);
        column4.add(2);
        column4.add(3);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");
        names.add("column3");
        names.add("column4");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        dataframe1.setVariable("a");
        dataframe1.setData(data);
        dataframe1.setNames(names);
        dataframe1.setRowNames(rowNames);

        //Test data
        Assert.assertEquals(3.0, engine.eval("a[1,1]"));
        Assert.assertEquals(4.5, engine.eval("a[2,1]"));
        Assert.assertEquals(0.01, engine.eval("a[3,1]"));
        Assert.assertEquals("titi", engine.eval("a[1,2]"));
        Assert.assertEquals("tata", engine.eval("a[2,2]"));
        Assert.assertEquals("toto", engine.eval("a[3,2]"));
        Assert.assertEquals(true, engine.eval("a[1,3]"));
        Assert.assertEquals(true, engine.eval("a[2,3]"));
        Assert.assertEquals(false, engine.eval("a[3,3]"));
        Assert.assertEquals(1, engine.eval("a[1,4]"));
        Assert.assertEquals(2, engine.eval("a[2,4]"));
        Assert.assertEquals(3, engine.eval("a[3,4]"));
        //Test names
        Assert.assertEquals("column1", engine.eval("names(a)[1]"));
        Assert.assertEquals("column2", engine.eval("names(a)[2]"));
        //Test row names
        Assert.assertEquals("row 1", engine.eval("row.names(a)[1]"));
        Assert.assertEquals("row 2", engine.eval("row.names(a)[2]"));
        Assert.assertEquals("row 3", engine.eval("row.names(a)[3]"));
    }

    @Test
    public void testDataFrameCreationMethod2() throws Exception {
        /*
         * This test data.frame creation method 2 and data assignment : 
         * RDataFrame(REngine, Object[], int) + all setters
         */

        Object[] datatypes = { 1.0, "a", true, 1 };

        RDataFrame dataframe1 = new RDataFrame(engine, datatypes, 3);

        dataframe1.setVariable("a");

        dataframe1.set(0, 0, 3.0);
        dataframe1.set(0, 1, 4.5);
        dataframe1.set(0, 2, 0.01);
        dataframe1.set(1, 0, "titi");
        dataframe1.set(1, 1, "tata");
        dataframe1.set(1, 2, "toto");
        dataframe1.set(2, 0, true);
        dataframe1.set(2, 1, true);
        dataframe1.set(2, 2, false);
        dataframe1.set(3, 0, 1);
        dataframe1.set(3, 1, 2);
        dataframe1.set(3, 2, 3);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        dataframe1.setNames(names);
        dataframe1.setRowNames(rowNames);

        //Test data
        Assert.assertEquals(3.0, engine.eval("a[1,1]"));
        Assert.assertEquals(4.5, engine.eval("a[2,1]"));
        Assert.assertEquals(0.01, engine.eval("a[3,1]"));
        Assert.assertEquals("titi", engine.eval("a[1,2]"));
        Assert.assertEquals("tata", engine.eval("a[2,2]"));
        Assert.assertEquals("toto", engine.eval("a[3,2]"));
        Assert.assertEquals(true, engine.eval("a[1,3]"));
        Assert.assertEquals(true, engine.eval("a[2,3]"));
        Assert.assertEquals(false, engine.eval("a[3,3]"));
        Assert.assertEquals(1, engine.eval("a[1,4]"));
        Assert.assertEquals(2, engine.eval("a[2,4]"));
        Assert.assertEquals(3, engine.eval("a[3,4]"));
        //Test names
        Assert.assertEquals("column1", engine.eval("names(a)[1]"));
        Assert.assertEquals("column2", engine.eval("names(a)[2]"));
        //Test row names
        Assert.assertEquals("row 1", engine.eval("row.names(a)[1]"));
        Assert.assertEquals("row 2", engine.eval("row.names(a)[2]"));
        Assert.assertEquals("row 3", engine.eval("row.names(a)[3]"));
    }

    @Test
    public void testDataFrameCreationMethod3() throws Exception {
        /*
         * This test data.frame creation method 3 and data assignment : 
         * RDataFrame(REngine, Object[], int) + all setters
         */

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        new RDataFrame(engine, names, rowNames, data, "a");

        //Test data
        Assert.assertEquals(3.0, engine.eval("a[1,1]"));
        Assert.assertEquals(4.5, engine.eval("a[2,1]"));
        Assert.assertEquals(0.01, engine.eval("a[3,1]"));
        Assert.assertEquals(1.0, engine.eval("a[1,2]"));
        Assert.assertEquals(5555555555555555555555.0, engine.eval("a[2,2]"));
        Assert.assertEquals(3.0, engine.eval("a[3,2]"));
        //Test names
        Assert.assertEquals("column1", engine.eval("names(a)[1]"));
        Assert.assertEquals("column2", engine.eval("names(a)[2]"));
        //Test row names
        Assert.assertEquals("row 1", engine.eval("row.names(a)[1]"));
        Assert.assertEquals("row 2", engine.eval("row.names(a)[2]"));
        Assert.assertEquals("row 3", engine.eval("row.names(a)[3]"));
    }

    @Test
    public void testGetSetNamesAndRowNames() throws Exception {
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        dataframe1.setVariable("a");
        dataframe1.setData(data);
        dataframe1.setNames(names);
        dataframe1.setRowNames(rowNames);

        //Test getName and getNames
        Assert.assertEquals("column1", dataframe1.getName(0));
        Assert.assertEquals("column2", dataframe1.getName(1));
        Assert.assertEquals("column1", dataframe1.getNames().get(0));
        Assert.assertEquals("column2", dataframe1.getNames().get(1));

        //Test getRowName and getRowNames
        Assert.assertEquals("row 1", dataframe1.getRowName(0));
        Assert.assertEquals("row 2", dataframe1.getRowName(1));
        Assert.assertEquals("row 1", dataframe1.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe1.getRowNames().get(1));

        //Test setRowName(y)
        dataframe1.setName(0, "toto");
        Assert.assertEquals("toto", dataframe1.getName(0));

        //Test setName(x)
        dataframe1.setRowName(0, "toto");
        Assert.assertEquals("toto", dataframe1.getRowName(0));

        //Test normally thrown exception when getting name(int)
        try {
            dataframe1.getName(2);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when getting rowname(int)
        try {
            dataframe1.getRowName(3);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting rowname(int,String)
        try {
            dataframe1.setRowName(3, "tata");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting name(int,String)
        try {
            dataframe1.setName(3, "tata");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when getting name(int)
        try {
            dataframe1.getRowName(3);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting names.
        List<String> bigNames = new ArrayList<String>();
        bigNames.add("column1");
        bigNames.add("column2");
        bigNames.add("column3");
        try {
            dataframe1.setNames(bigNames);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting rownames.
        List<String> bigRowNames = new ArrayList<String>();
        bigRowNames.add("row1");
        bigRowNames.add("row2");
        bigRowNames.add("row3");
        bigRowNames.add("row4");
        try {
            dataframe1.setRowNames(bigRowNames);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Modify the remote data.frame so we can test normally thrown exceptions
        //when getting names and row names
        engine.voidEval("a<-data.frame(" +
                "a=c(1,2,3,4)," +
                "b=c(4,5,6,7)," +
                "d=c(7,8,9,10)," +
                "row.names=c(\"a\",\"b\",\"c\",\"d\"))");

        try {
            dataframe1.getNames();
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        try {
            dataframe1.getRowNames();
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }
    }

    @Test
    public void testGettersSettersLocationXYAllTypes() throws Exception {
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Integer> column2 = new ArrayList<Integer>();
        column2.add(1);
        column2.add(555555555);
        column2.add(3);

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<String> column4 = new ArrayList<String>();
        column4.add("titi");
        column4.add("toto");
        column4.add("tata");

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        dataframe1.setVariable("a");
        dataframe1.setData(data);

        //Test all getters
        Assert.assertEquals(3.0, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(1, dataframe1.get(1, 0));
        Assert.assertEquals(555555555, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(true, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(false, dataframe1.get(2, 2));

        Assert.assertEquals("titi", dataframe1.get(3, 0));
        Assert.assertEquals("toto", dataframe1.get(3, 1));
        Assert.assertEquals("tata", dataframe1.get(3, 2));

        //Test exceptions normally thrown using getters
        try {
            dataframe1.get(5, 7);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        try {
            dataframe1.get(1, 7);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        try {
            dataframe1.get(5, 1);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test setters
        dataframe1.set(0, 0, 45.6);
        dataframe1.set(1, 0, 45);
        dataframe1.set(2, 0, false);
        dataframe1.set(2, 2, true);
        dataframe1.set(3, 0, "tonton");

        Assert.assertEquals(45.6, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(45, dataframe1.get(1, 0));
        Assert.assertEquals(555555555, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(false, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(true, dataframe1.get(2, 2));

        Assert.assertEquals("tonton", dataframe1.get(3, 0));
        Assert.assertEquals("toto", dataframe1.get(3, 1));
        Assert.assertEquals("tata", dataframe1.get(3, 2));

        //Test that exceptions are normally thrown

        //x and y too big
        try {
            dataframe1.set(5, 7, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //y too big
        try {
            dataframe1.set(1, 7, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //x too big
        try {
            dataframe1.set(5, 1, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set a Double instead of Integer
        try {
            dataframe1.set(1, 0, 45.6);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set a Double instead of a Boolean
        try {
            dataframe1.set(2, 0, 45.6);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set a Double instead of a String
        try {
            dataframe1.set(3, 0, 45.6);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set an Integer instead of a Double
        try {
            dataframe1.set(0, 0, 45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set an Integer instead of a Boolean
        try {
            dataframe1.set(2, 0, 45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set an Integer instead of a String
        try {
            dataframe1.set(3, 0, 45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a Boolean instead of a Double
        try {
            dataframe1.set(0, 0, false);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a Boolean instead of an Integer
        try {
            dataframe1.set(1, 0, false);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a Boolean instead of a String 
        try {
            dataframe1.set(3, 0, false);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a String instead of a Double
        try {
            dataframe1.set(0, 0, "tonton");
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a String instead of an Integer
        try {
            dataframe1.set(1, 0, "tonton");
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a String instead of a Boolean
        try {
            dataframe1.set(2, 0, "tonton");
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //variable not existing String
        try {
            dataframe1.set(5, 7, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }
    }

    @Test
    public void testSetGetVariable() throws Exception {
        RDataFrame dataframe1 = new RDataFrame(engine);
        dataframe1.setVariable("a");
        Assert.assertEquals("a", dataframe1.getVariable());
    }

    @Test
    public void testSetGetEngine() throws Exception {
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Integer> column2 = new ArrayList<Integer>();
        column2.add(1);
        column2.add(555555555);
        column2.add(3);

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<String> column4 = new ArrayList<String>();
        column4.add("titi");
        column4.add("toto");
        column4.add("tata");

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        dataframe1.setVariable("a");
        dataframe1.setData(data);

        REngine secondEngine = new RProxy();
        dataframe1.setEngine(secondEngine);

        //Test that the values are still OK
        Assert.assertEquals(3.0, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(1, dataframe1.get(1, 0));
        Assert.assertEquals(555555555, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(true, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(false, dataframe1.get(2, 2));

        Assert.assertEquals("titi", dataframe1.get(3, 0));
        Assert.assertEquals("toto", dataframe1.get(3, 1));
        Assert.assertEquals("tata", dataframe1.get(3, 2));

        //Check that the engine is not the old engine
        Assert.assertNotSame(engine, dataframe1.getEngine());

        //Check that the engine is the new one
        Assert.assertEquals(secondEngine, dataframe1.getEngine());

    }

    @Test
    public void testGetSetAttributes() throws Exception {
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Integer> column2 = new ArrayList<Integer>();
        column2.add(1);
        column2.add(555555555);
        column2.add(3);

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<String> column4 = new ArrayList<String>();
        column4.add("titi");
        column4.add("toto");
        column4.add("tata");

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        dataframe1.setVariable("a");
        dataframe1.setData(data);

        dataframe1.setAttribute("test_attribute", "test_value");
        dataframe1.setAttribute("second_test_attribute", "second_value");

        Assert.assertTrue((Boolean) engine.eval("is.data.frame(a)"));
        Assert.assertEquals("test_value",
                engine.eval("attributes(a)$test_attribute"));
        Assert.assertEquals("second_value",
                engine.eval("attributes(a)$second_test_attribute"));
        Assert.assertEquals("test_value",
                dataframe1.getAttribute("test_attribute"));
        Assert.assertEquals("second_value",
                dataframe1.getAttribute("second_test_attribute"));

        dataframe1.setAttribute("test_attribute", "third_value");

        Assert.assertEquals("third_value",
                engine.eval("attributes(a)$test_attribute"));

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("attr1", "this is an attribute");
        attributes.put("attr2", "this is another attribute");
        dataframe1.setAttributes(attributes);

        Assert.assertTrue((Boolean) engine.eval("is.data.frame(a)"));
        Assert.assertEquals("this is an attribute",
                dataframe1.getAttribute("attr1"));
        Assert.assertEquals("this is an attribute",
                dataframe1.getAttribute("attr1"));
        Assert.assertEquals("this is another attribute",
                dataframe1.getAttribute("attr2"));
        Assert.assertEquals("this is an attribute",
                dataframe1.getAttributes().get("attr1"));
        Assert.assertEquals("this is another attribute",
                dataframe1.getAttributes().get("attr2"));

        try {
            dataframe1.getAttribute("toto");
            Assert.fail("Should have thrown an RException at this point");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);

        }
    }

    @Test
    public void testImportExportCsv() throws Exception {

        //Test import with same type for each column (Double).

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        RDataFrame testDataFrame = new RDataFrame(engine);
        try {
            testDataFrame = new RDataFrame(engine, names, rowNames, data,
                    "test");
        } catch (RException eee) {
            Assert.fail();
        }
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        RDataFrame dataframe2 = new RDataFrame(engine);
        dataframe2.importCsv(new File("/tmp/test.csv"), true, true, 3.0);
        dataframe2.setVariable("a");

        List<List<?>> data2 = dataframe2.getData();

        //Test data
        Assert.assertEquals(3.0, data2.get(0).get(0));
        Assert.assertEquals(4.5, data2.get(0).get(1));
        Assert.assertEquals(0.01, data2.get(0).get(2));
        Assert.assertEquals(1.0, data2.get(1).get(0));
        Assert.assertEquals(5555555555555555555555.0, data2.get(1).get(1));
        Assert.assertEquals(3.0, data2.get(1).get(2));
        //Test names
        Assert.assertEquals("column1", dataframe2.getNames().get(0));
        Assert.assertEquals("column2", dataframe2.getNames().get(1));
        //Test row names
        Assert.assertEquals("row 1", dataframe2.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe2.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe2.getRowNames().get(2));

        //Test import with same type for each column (String).

        names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<String> column5 = new ArrayList<String>();
        column5.add("bla");
        column5.add("blabla");
        column5.add("blablabla");

        List<String> column6 = new ArrayList<String>();
        column6.add("titi");
        column6.add("tata");
        column6.add("toto");

        data = new ArrayList<List<?>>();
        data.add(column5);
        data.add(column6);

        testDataFrame = new RDataFrame(engine, names, rowNames, data, "test");
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        dataframe2 = new RDataFrame(engine);
        dataframe2.importCsv(new File("/tmp/test.csv"), true, true, "a");
        dataframe2.setVariable("a");

        //Test data
        Assert.assertEquals("bla", dataframe2.getData().get(0).get(0));
        Assert.assertEquals("blabla", dataframe2.getData().get(0).get(1));
        Assert.assertEquals("blablabla", dataframe2.getData().get(0).get(2));
        Assert.assertEquals("titi", dataframe2.getData().get(1).get(0));
        Assert.assertEquals("tata", dataframe2.getData().get(1).get(1));
        Assert.assertEquals("toto", dataframe2.getData().get(1).get(2));
        //Test names
        Assert.assertEquals("column1", dataframe2.getNames().get(0));
        Assert.assertEquals("column2", dataframe2.getNames().get(1));
        //Test row names
        Assert.assertEquals("row 1", dataframe2.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe2.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe2.getRowNames().get(2));

        //Test import with same type for each column (String).

        names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Integer> column7 = new ArrayList<Integer>();
        column7.add(1);
        column7.add(2);
        column7.add(3);

        List<Integer> column8 = new ArrayList<Integer>();
        column8.add(4);
        column8.add(5);
        column8.add(6);

        data = new ArrayList<List<?>>();
        data.add(column7);
        data.add(column8);

        testDataFrame = new RDataFrame(engine, names, rowNames, data, "test");
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        dataframe2 = new RDataFrame(engine);
        dataframe2.importCsv(new File("/tmp/test.csv"), true, true, 12);
        dataframe2.setVariable("a");

        //Test data
        Assert.assertEquals(1, dataframe2.getData().get(0).get(0));
        Assert.assertEquals(2, dataframe2.getData().get(0).get(1));
        Assert.assertEquals(3, dataframe2.getData().get(0).get(2));
        Assert.assertEquals(4, dataframe2.getData().get(1).get(0));
        Assert.assertEquals(5, dataframe2.getData().get(1).get(1));
        Assert.assertEquals(6, dataframe2.getData().get(1).get(2));
        //Test names
        Assert.assertEquals("column1", dataframe2.getNames().get(0));
        Assert.assertEquals("column2", dataframe2.getNames().get(1));
        //Test row names
        Assert.assertEquals("row 1", dataframe2.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe2.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe2.getRowNames().get(2));

        //Test import with different types for each column        

        names = new ArrayList<String>();
        names.add("column1");
        names.add("column3");
        names.add("column4");

        rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        ArrayList<Integer> column3 = new ArrayList<Integer>();
        column3.add(1);
        column3.add(5);
        column3.add(3);

        ArrayList<String> column4 = new ArrayList<String>();
        column4.add("bla");
        column4.add("blabla");
        column4.add("blablabla");

        data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column3);
        data.add(column4);

        List<Object> types = new ArrayList<Object>();
        types.add(4.0);
        types.add(4);
        types.add("");

        testDataFrame = new RDataFrame(engine);
        try {
            testDataFrame = new RDataFrame(engine, names, rowNames, data,
                    "test");
        } catch (RException eee) {
            Assert.fail();
        }
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        RDataFrame dataframe3 = new RDataFrame(engine);
        dataframe3.importCsv(new File("/tmp/test.csv"), true, true, types);
        dataframe3.setVariable("a");

        //Test data
        Assert.assertEquals(3.0, dataframe3.getData().get(0).get(0));
        Assert.assertEquals(4.5, dataframe3.getData().get(0).get(1));
        Assert.assertEquals(0.01, dataframe3.getData().get(0).get(2));
        Assert.assertEquals(1, dataframe3.getData().get(1).get(0));
        Assert.assertEquals(5, dataframe3.getData().get(1).get(1));
        Assert.assertEquals(3, dataframe3.getData().get(1).get(2));
        Assert.assertEquals("bla", dataframe3.getData().get(2).get(0));
        Assert.assertEquals("blabla", dataframe3.getData().get(2).get(1));
        Assert.assertEquals("blablabla", dataframe3.getData().get(2).get(2));
        //Test names
        Assert.assertEquals("column1", dataframe3.getNames().get(0));
        Assert.assertEquals("column3", dataframe3.getNames().get(1));
        Assert.assertEquals("column4", dataframe3.getNames().get(2));
        //Test row names
        Assert.assertEquals("row 1", dataframe3.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe3.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe3.getRowNames().get(2));

        //Test import without precision on type

        RDataFrame dataframe4 = new RDataFrame(engine);
        dataframe4.importCsv(new File("/tmp/test.csv"), true, true);
        dataframe4.setVariable("a");

        //Test data
        Assert.assertEquals("3.0", dataframe4.getData().get(0).get(0));
        Assert.assertEquals("4.5", dataframe4.getData().get(0).get(1));
        Assert.assertEquals("0.01", dataframe4.getData().get(0).get(2));
        Assert.assertEquals("1", dataframe4.getData().get(1).get(0));
        Assert.assertEquals("5", dataframe4.getData().get(1).get(1));
        Assert.assertEquals("3", dataframe4.getData().get(1).get(2));
        Assert.assertEquals("bla", dataframe4.getData().get(2).get(0));
        Assert.assertEquals("blabla", dataframe4.getData().get(2).get(1));
        Assert.assertEquals("blablabla", dataframe4.getData().get(2).get(2));
        //Test names
        Assert.assertEquals("column1", dataframe4.getNames().get(0));
        Assert.assertEquals("column3", dataframe4.getNames().get(1));
        Assert.assertEquals("column4", dataframe4.getNames().get(2));
        //Test row names
        Assert.assertEquals("row 1", dataframe4.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe4.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe4.getRowNames().get(2));

    }

    @Test
    public void testGetFrom() throws Exception {
        engine.voidEval("a<-data.frame(" +
                "column1=c(3.0,4.5,0.01)," +
                "column2=c(as.integer(1),as.integer(5),as.integer(3))," +
                "column3=c(TRUE,TRUE,FALSE)," +
                "column4=c(\"bla\",\"blabla\",\"blablabla\")," +
                "row.names=c(\"row 1\",\"row 2\",\"row 3\")," +
                "stringsAsFactors=FALSE)");
        RDataFrame dataframe1 = new RDataFrame(engine);
        dataframe1.getFrom("a");

        //Test that the values are still OK
        Assert.assertEquals(3.0, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(1, dataframe1.get(1, 0));
        Assert.assertEquals(5, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(true, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(false, dataframe1.get(2, 2));

        Assert.assertEquals("bla", dataframe1.get(3, 0));
        Assert.assertEquals("blabla", dataframe1.get(3, 1));
        Assert.assertEquals("blablabla", dataframe1.get(3, 2));

        //Test names
        Assert.assertEquals("column1", dataframe1.getNames().get(0));
        Assert.assertEquals("column2", dataframe1.getNames().get(1));
        Assert.assertEquals("column3", dataframe1.getNames().get(2));
        Assert.assertEquals("column4", dataframe1.getNames().get(3));
        //Test row names
        Assert.assertEquals("row 1", dataframe1.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe1.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe1.getRowNames().get(2));
    }

    @Test
    public void testDataFrameCreationMethod1WithoutAutoCommit()
            throws Exception {
        /*
         * This test data.frame creation method 1 and data assignment : 
         * RDataFrame(REngine) + all setters
         */

        engine.setAutoCommit(false);
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<String> column2 = new ArrayList<String>();
        column2.add("titi");
        column2.add("tata");
        column2.add("toto");

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<Integer> column4 = new ArrayList<Integer>();
        column4.add(1);
        column4.add(2);
        column4.add(3);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");
        names.add("column3");
        names.add("column4");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        dataframe1.setVariable("a");
        dataframe1.setData(data);
        dataframe1.setNames(names);
        dataframe1.setRowNames(rowNames);

        engine.commit();
        engine.setAutoCommit(true);

        //Test data
        Assert.assertEquals(3.0, engine.eval("a[1,1]"));
        Assert.assertEquals(4.5, engine.eval("a[2,1]"));
        Assert.assertEquals(0.01, engine.eval("a[3,1]"));
        Assert.assertEquals("titi", engine.eval("a[1,2]"));
        Assert.assertEquals("tata", engine.eval("a[2,2]"));
        Assert.assertEquals("toto", engine.eval("a[3,2]"));
        Assert.assertEquals(true, engine.eval("a[1,3]"));
        Assert.assertEquals(true, engine.eval("a[2,3]"));
        Assert.assertEquals(false, engine.eval("a[3,3]"));
        Assert.assertEquals(1, engine.eval("a[1,4]"));
        Assert.assertEquals(2, engine.eval("a[2,4]"));
        Assert.assertEquals(3, engine.eval("a[3,4]"));
        //Test names
        Assert.assertEquals("column1", engine.eval("names(a)[1]"));
        Assert.assertEquals("column2", engine.eval("names(a)[2]"));
        //Test row names
        Assert.assertEquals("row 1", engine.eval("row.names(a)[1]"));
        Assert.assertEquals("row 2", engine.eval("row.names(a)[2]"));
        Assert.assertEquals("row 3", engine.eval("row.names(a)[3]"));
    }

    @Test
    public void testDataFrameCreationMethod2WithoutAutoCommit()
            throws Exception {
        /*
         * This test data.frame creation method 2 and data assignment : 
         * RDataFrame(REngine, Object[], int) + all setters
         */

        engine.setAutoCommit(false);
        Object[] datatypes = { 1.0, "a", true, 1 };

        RDataFrame dataframe1 = new RDataFrame(engine, datatypes, 3);

        dataframe1.setVariable("a");

        dataframe1.set(0, 0, 3.0);
        dataframe1.set(0, 1, 4.5);
        dataframe1.set(0, 2, 0.01);
        dataframe1.set(1, 0, "titi");
        dataframe1.set(1, 1, "tata");
        dataframe1.set(1, 2, "toto");
        dataframe1.set(2, 0, true);
        dataframe1.set(2, 1, true);
        dataframe1.set(2, 2, false);
        dataframe1.set(3, 0, 1);
        dataframe1.set(3, 1, 2);
        dataframe1.set(3, 2, 3);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        dataframe1.setNames(names);
        dataframe1.setRowNames(rowNames);

        engine.commit();
        engine.setAutoCommit(true);

        //Test data
        Assert.assertEquals(3.0, engine.eval("a[1,1]"));
        Assert.assertEquals(4.5, engine.eval("a[2,1]"));
        Assert.assertEquals(0.01, engine.eval("a[3,1]"));
        Assert.assertEquals("titi", engine.eval("a[1,2]"));
        Assert.assertEquals("tata", engine.eval("a[2,2]"));
        Assert.assertEquals("toto", engine.eval("a[3,2]"));
        Assert.assertEquals(true, engine.eval("a[1,3]"));
        Assert.assertEquals(true, engine.eval("a[2,3]"));
        Assert.assertEquals(false, engine.eval("a[3,3]"));
        Assert.assertEquals(1, engine.eval("a[1,4]"));
        Assert.assertEquals(2, engine.eval("a[2,4]"));
        Assert.assertEquals(3, engine.eval("a[3,4]"));
        //Test names
        Assert.assertEquals("column1", engine.eval("names(a)[1]"));
        Assert.assertEquals("column2", engine.eval("names(a)[2]"));
        //Test row names
        Assert.assertEquals("row 1", engine.eval("row.names(a)[1]"));
        Assert.assertEquals("row 2", engine.eval("row.names(a)[2]"));
        Assert.assertEquals("row 3", engine.eval("row.names(a)[3]"));
    }

    @Test
    public void testDataFrameCreationMethod3WithoutAutoCommit()
            throws Exception {
        /*
         * This test data.frame creation method 3 and data assignment : 
         * RDataFrame(REngine, Object[], int) + all setters
         */

        engine.setAutoCommit(false);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        new RDataFrame(engine, names, rowNames, data, "a");

        engine.commit();
        engine.setAutoCommit(true);

        //Test data
        Assert.assertEquals(3.0, engine.eval("a[1,1]"));
        Assert.assertEquals(4.5, engine.eval("a[2,1]"));
        Assert.assertEquals(0.01, engine.eval("a[3,1]"));
        Assert.assertEquals(1.0, engine.eval("a[1,2]"));
        Assert.assertEquals(5555555555555555555555.0, engine.eval("a[2,2]"));
        Assert.assertEquals(3.0, engine.eval("a[3,2]"));
        //Test names
        Assert.assertEquals("column1", engine.eval("names(a)[1]"));
        Assert.assertEquals("column2", engine.eval("names(a)[2]"));
        //Test row names
        Assert.assertEquals("row 1", engine.eval("row.names(a)[1]"));
        Assert.assertEquals("row 2", engine.eval("row.names(a)[2]"));
        Assert.assertEquals("row 3", engine.eval("row.names(a)[3]"));
    }

    @Test
    public void testGetSetNamesAndRowNamesWithoutAutoCommit() throws Exception {

        engine.setAutoCommit(false);

        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        dataframe1.setVariable("a");
        dataframe1.setData(data);
        dataframe1.setNames(names);
        dataframe1.setRowNames(rowNames);

        //Test getName and getNames
        Assert.assertEquals("column1", dataframe1.getName(0));
        Assert.assertEquals("column2", dataframe1.getName(1));
        Assert.assertEquals("column1", dataframe1.getNames().get(0));
        Assert.assertEquals("column2", dataframe1.getNames().get(1));

        //Test getRowName and getRowNames
        Assert.assertEquals("row 1", dataframe1.getRowName(0));
        Assert.assertEquals("row 2", dataframe1.getRowName(1));
        Assert.assertEquals("row 1", dataframe1.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe1.getRowNames().get(1));

        //Test setRowName(y)
        dataframe1.setName(0, "toto");
        Assert.assertEquals("toto", dataframe1.getName(0));

        //Test setName(x)
        dataframe1.setRowName(0, "toto");
        Assert.assertEquals("toto", dataframe1.getRowName(0));

        //Test normally thrown exception when getting name(int)
        try {
            dataframe1.getName(2);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when getting rowname(int)
        try {
            dataframe1.getRowName(3);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting rowname(int,String)
        try {
            dataframe1.setRowName(3, "tata");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting name(int,String)
        try {
            dataframe1.setName(3, "tata");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when getting name(int)
        try {
            dataframe1.getRowName(3);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting names.
        List<String> bigNames = new ArrayList<String>();
        bigNames.add("column1");
        bigNames.add("column2");
        bigNames.add("column3");
        try {
            dataframe1.setNames(bigNames);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test normally thrown exception when setting rownames.
        List<String> bigRowNames = new ArrayList<String>();
        bigRowNames.add("row1");
        bigRowNames.add("row2");
        bigRowNames.add("row3");
        bigRowNames.add("row4");
        try {
            dataframe1.setRowNames(bigRowNames);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        engine.setAutoCommit(true);
    }

    @Test
    public void testGettersSettersLocationXYAllTypesWithoutAutoCommit()
            throws Exception {
        engine.setAutoCommit(false);
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Integer> column2 = new ArrayList<Integer>();
        column2.add(1);
        column2.add(555555555);
        column2.add(3);

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<String> column4 = new ArrayList<String>();
        column4.add("titi");
        column4.add("toto");
        column4.add("tata");

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        dataframe1.setVariable("a");
        dataframe1.setData(data);

        //Test all getters
        Assert.assertEquals(3.0, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(1, dataframe1.get(1, 0));
        Assert.assertEquals(555555555, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(true, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(false, dataframe1.get(2, 2));

        Assert.assertEquals("titi", dataframe1.get(3, 0));
        Assert.assertEquals("toto", dataframe1.get(3, 1));
        Assert.assertEquals("tata", dataframe1.get(3, 2));

        //Test exceptions normally thrown using getters
        try {
            dataframe1.get(5, 7);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        try {
            dataframe1.get(1, 7);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        try {
            dataframe1.get(5, 1);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Test setters
        dataframe1.set(0, 0, 45.6);
        dataframe1.set(1, 0, 45);
        dataframe1.set(2, 0, false);
        dataframe1.set(2, 2, true);
        dataframe1.set(3, 0, "tonton");

        Assert.assertEquals(45.6, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(45, dataframe1.get(1, 0));
        Assert.assertEquals(555555555, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(false, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(true, dataframe1.get(2, 2));

        Assert.assertEquals("tonton", dataframe1.get(3, 0));
        Assert.assertEquals("toto", dataframe1.get(3, 1));
        Assert.assertEquals("tata", dataframe1.get(3, 2));

        //Test that exceptions are normally thrown

        //x and y too big String
        try {
            dataframe1.set(5, 7, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //y too big String
        try {
            dataframe1.set(1, 7, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //x too big String
        try {
            dataframe1.set(5, 1, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set a Double instead of Integer
        try {
            dataframe1.set(1, 0, 45.6);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set a Double instead of a Boolean
        try {
            dataframe1.set(2, 0, 45.6);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set a Double instead of a String
        try {
            dataframe1.set(3, 0, 45.6);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set an Integer instead of a Double
        try {
            dataframe1.set(0, 0, 45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set an Integer instead of a Boolean
        try {
            dataframe1.set(2, 0, 45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //Set an Integer instead of a String
        try {
            dataframe1.set(3, 0, 45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a Boolean instead of a Double
        try {
            dataframe1.set(0, 0, false);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a Boolean instead of an Integer
        try {
            dataframe1.set(1, 0, false);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a Boolean instead of a String 
        try {
            dataframe1.set(3, 0, false);
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a String instead of a Double
        try {
            dataframe1.set(0, 0, "tonton");
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a String instead of an Integer
        try {
            dataframe1.set(1, 0, "tonton");
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
        }

        //Set a String instead of a Boolean
        try {
            dataframe1.set(2, 0, "tonton");
            Assert.fail("Error was not thrown although it should have been");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);
        }

        //variable not existing String
        try {
            dataframe1.set(5, 7, "blabla");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
            log.debug("Exception normally thrown ", eee);
        }
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetGetVariableWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RDataFrame dataframe1 = new RDataFrame(engine);
        dataframe1.setVariable("a");
        Assert.assertEquals("a", dataframe1.getVariable());
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetGetEngineWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);

        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Integer> column2 = new ArrayList<Integer>();
        column2.add(1);
        column2.add(555555555);
        column2.add(3);

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<String> column4 = new ArrayList<String>();
        column4.add("titi");
        column4.add("toto");
        column4.add("tata");

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        dataframe1.setVariable("a");
        dataframe1.setData(data);

        REngine secondEngine = new RProxy();
        dataframe1.setEngine(secondEngine);

        //Test that the values are still OK
        Assert.assertEquals(3.0, dataframe1.get(0, 0));
        Assert.assertEquals(4.5, dataframe1.get(0, 1));
        Assert.assertEquals(0.01, dataframe1.get(0, 2));

        Assert.assertEquals(1, dataframe1.get(1, 0));
        Assert.assertEquals(555555555, dataframe1.get(1, 1));
        Assert.assertEquals(3, dataframe1.get(1, 2));

        Assert.assertEquals(true, dataframe1.get(2, 0));
        Assert.assertEquals(true, dataframe1.get(2, 1));
        Assert.assertEquals(false, dataframe1.get(2, 2));

        Assert.assertEquals("titi", dataframe1.get(3, 0));
        Assert.assertEquals("toto", dataframe1.get(3, 1));
        Assert.assertEquals("tata", dataframe1.get(3, 2));

        //Check that the engine is not the old engine
        Assert.assertNotSame(engine, dataframe1.getEngine());

        //Check that the engine is the new one
        Assert.assertEquals(secondEngine, dataframe1.getEngine());
        engine.setAutoCommit(true);

    }

    @Test
    public void testGetSetAttributesWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RDataFrame dataframe1 = new RDataFrame(engine);

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Integer> column2 = new ArrayList<Integer>();
        column2.add(1);
        column2.add(555555555);
        column2.add(3);

        List<Boolean> column3 = new ArrayList<Boolean>();
        column3.add(true);
        column3.add(true);
        column3.add(false);

        List<String> column4 = new ArrayList<String>();
        column4.add("titi");
        column4.add("toto");
        column4.add("tata");

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);
        data.add(column3);
        data.add(column4);

        dataframe1.setVariable("a");
        dataframe1.setData(data);

        dataframe1.setAttribute("test_attribute", "test_value");
        dataframe1.setAttribute("second_test_attribute", "second_value");

        engine.setAutoCommit(true);
        Assert.assertTrue((Boolean) engine.eval("is.data.frame(a)"));
        Assert.assertEquals("test_value",
                engine.eval("attributes(a)$test_attribute"));
        Assert.assertEquals("second_value",
                engine.eval("attributes(a)$second_test_attribute"));
        engine.setAutoCommit(false);
        Assert.assertEquals("test_value",
                dataframe1.getAttribute("test_attribute"));
        Assert.assertEquals("second_value",
                dataframe1.getAttribute("second_test_attribute"));

        dataframe1.setAttribute("test_attribute", "third_value");

        engine.setAutoCommit(true);
        Assert.assertTrue((Boolean) engine.eval("is.data.frame(a)"));
        Assert.assertEquals("third_value",
                engine.eval("attributes(a)$test_attribute"));
        engine.setAutoCommit(false);

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("attr1", "this is an attribute");
        attributes.put("attr2", "this is another attribute");
        dataframe1.setAttributes(attributes);

        Assert.assertTrue((Boolean) engine.eval("is.data.frame(a)"));
        Assert.assertEquals("this is an attribute",
                dataframe1.getAttribute("attr1"));
        Assert.assertEquals("this is an attribute",
                dataframe1.getAttribute("attr1"));
        Assert.assertEquals("this is another attribute",
                dataframe1.getAttribute("attr2"));
        Assert.assertEquals("this is an attribute",
                dataframe1.getAttributes().get("attr1"));
        Assert.assertEquals("this is another attribute",
                dataframe1.getAttributes().get("attr2"));
        engine.setAutoCommit(true);
    }

    @Test
    public void testImportExportCsvWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        //Test import with same type for each column (Double).

        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        RDataFrame testDataFrame = new RDataFrame(engine);
        try {
            testDataFrame = new RDataFrame(engine, names, rowNames, data,
                    "test");
        } catch (RException eee) {
            Assert.fail();
        }
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        RDataFrame dataframe2 = new RDataFrame(engine);
        dataframe2.importCsv(new File("/tmp/test.csv"), true, true, 3.0);
        dataframe2.setVariable("a");

        //Test data
        Assert.assertEquals(3.0, dataframe2.get(0, 0));
        Assert.assertEquals(4.5, dataframe2.get(0, 1));
        Assert.assertEquals(0.01, dataframe2.get(0, 2));
        Assert.assertEquals(1.0, dataframe2.get(1, 0));
        Assert.assertEquals(5555555555555555555555.0, dataframe2.get(1, 1));
        Assert.assertEquals(3.0, dataframe2.get(1, 2));
        //Test names
        Assert.assertEquals("column1", dataframe2.getNames().get(0));
        Assert.assertEquals("column2", dataframe2.getNames().get(1));
        //Test row names
        Assert.assertEquals("row 1", dataframe2.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe2.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe2.getRowNames().get(2));

        //Test import with same type for each column (String).

        names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<String> column5 = new ArrayList<String>();
        column5.add("bla");
        column5.add("blabla");
        column5.add("blablabla");

        List<String> column6 = new ArrayList<String>();
        column6.add("titi");
        column6.add("tata");
        column6.add("toto");

        data = new ArrayList<List<?>>();
        data.add(column5);
        data.add(column6);

        testDataFrame = new RDataFrame(engine, names, rowNames, data, "test");
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        dataframe2 = new RDataFrame(engine);
        dataframe2.importCsv(new File("/tmp/test.csv"), true, true, "a");
        dataframe2.setVariable("a");

        //Test data
        Assert.assertEquals("bla", dataframe2.get(0, 0));
        Assert.assertEquals("blabla", dataframe2.get(0, 1));
        Assert.assertEquals("blablabla", dataframe2.get(0, 2));
        Assert.assertEquals("titi", dataframe2.get(1, 0));
        Assert.assertEquals("tata", dataframe2.get(1, 1));
        Assert.assertEquals("toto", dataframe2.get(1, 2));
        //Test names
        Assert.assertEquals("column1", dataframe2.getNames().get(0));
        Assert.assertEquals("column2", dataframe2.getNames().get(1));
        //Test row names
        Assert.assertEquals("row 1", dataframe2.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe2.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe2.getRowNames().get(2));

        //Test import with same type for each column (Integer).

        names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Integer> column7 = new ArrayList<Integer>();
        column7.add(1);
        column7.add(2);
        column7.add(3);

        List<Integer> column8 = new ArrayList<Integer>();
        column8.add(4);
        column8.add(5);
        column8.add(6);

        data = new ArrayList<List<?>>();
        data.add(column7);
        data.add(column8);

        testDataFrame = new RDataFrame(engine, names, rowNames, data, "test");
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        dataframe2 = new RDataFrame(engine);
        dataframe2.importCsv(new File("/tmp/test.csv"), true, true, 12);
        dataframe2.setVariable("a");

        //Test data
        Assert.assertEquals(1, dataframe2.get(0, 0));
        Assert.assertEquals(2, dataframe2.get(0, 1));
        Assert.assertEquals(3, dataframe2.get(0, 2));
        Assert.assertEquals(4, dataframe2.get(1, 0));
        Assert.assertEquals(5, dataframe2.get(1, 1));
        Assert.assertEquals(6, dataframe2.get(1, 2));
        //Test names
        Assert.assertEquals("column1", dataframe2.getNames().get(0));
        Assert.assertEquals("column2", dataframe2.getNames().get(1));
        //Test row names
        Assert.assertEquals("row 1", dataframe2.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe2.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe2.getRowNames().get(2));

        //Test import with different types for each column        

        names = new ArrayList<String>();
        names.add("column1");
        names.add("column3");
        names.add("column4");

        rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        ArrayList<Integer> column3 = new ArrayList<Integer>();
        column3.add(1);
        column3.add(5);
        column3.add(3);

        ArrayList<String> column4 = new ArrayList<String>();
        column4.add("bla");
        column4.add("blabla");
        column4.add("blablabla");

        data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column3);
        data.add(column4);

        List<Object> types = new ArrayList<Object>();
        types.add(4.0);
        types.add(4);
        types.add("");

        testDataFrame = new RDataFrame(engine);
        try {
            testDataFrame = new RDataFrame(engine, names, rowNames, data,
                    "test");
        } catch (RException eee) {
            Assert.fail();
        }
        testDataFrame.exportCsv(new File("/tmp/test.csv"), true, true);

        RDataFrame dataframe3 = new RDataFrame(engine);
        dataframe3.importCsv(new File("/tmp/test.csv"), true, true, types);
        dataframe3.setVariable("a");

        //Test data
        Assert.assertEquals(3.0, dataframe3.get(0, 0));
        Assert.assertEquals(4.5, dataframe3.get(0, 1));
        Assert.assertEquals(0.01, dataframe3.get(0, 2));
        Assert.assertEquals(1, dataframe3.get(1, 0));
        Assert.assertEquals(5, dataframe3.get(1, 1));
        Assert.assertEquals(3, dataframe3.get(1, 2));
        Assert.assertEquals("bla", dataframe3.get(2, 0));
        Assert.assertEquals("blabla", dataframe3.get(2, 1));
        Assert.assertEquals("blablabla", dataframe3.get(2, 2));
        //Test names
        Assert.assertEquals("column1", dataframe3.getNames().get(0));
        Assert.assertEquals("column3", dataframe3.getNames().get(1));
        Assert.assertEquals("column4", dataframe3.getNames().get(2));
        //Test row names
        Assert.assertEquals("row 1", dataframe3.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe3.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe3.getRowNames().get(2));

        //Test import without precision on type

        RDataFrame dataframe4 = new RDataFrame(engine);
        dataframe4.importCsv(new File("/tmp/test.csv"), true, true);
        dataframe4.setVariable("a");

        //Test data
        Assert.assertEquals("3.0", dataframe4.get(0, 0));
        Assert.assertEquals("4.5", dataframe4.get(0, 1));
        Assert.assertEquals("0.01", dataframe4.get(0, 2));
        Assert.assertEquals("1", dataframe4.get(1, 0));
        Assert.assertEquals("5", dataframe4.get(1, 1));
        Assert.assertEquals("3", dataframe4.get(1, 2));
        Assert.assertEquals("bla", dataframe4.get(2, 0));
        Assert.assertEquals("blabla", dataframe4.get(2, 1));
        Assert.assertEquals("blablabla", dataframe4.get(2, 2));
        //Test names
        Assert.assertEquals("column1", dataframe4.getNames().get(0));
        Assert.assertEquals("column3", dataframe4.getNames().get(1));
        Assert.assertEquals("column4", dataframe4.getNames().get(2));
        //Test row names
        Assert.assertEquals("row 1", dataframe4.getRowNames().get(0));
        Assert.assertEquals("row 2", dataframe4.getRowNames().get(1));
        Assert.assertEquals("row 3", dataframe4.getRowNames().get(2));

        engine.setAutoCommit(true);
    }

    public void testDim (){
        List<String> names = new ArrayList<String>();
        names.add("column1");
        names.add("column2");

        List<String> rowNames = new ArrayList<String>();
        rowNames.add("row 1");
        rowNames.add("row 2");
        rowNames.add("row 3");

        List<Double> column1 = new ArrayList<Double>();
        column1.add(3.0);
        column1.add(4.5);
        column1.add(0.01);

        List<Double> column2 = new ArrayList<Double>();
        column2.add(1.0);
        column2.add(5555555555555555555555.0);
        column2.add(3.0);

        List<List<?>> data = new ArrayList<List<?>>();
        data.add(column1);
        data.add(column2);

        RDataFrame testDataFrame;
        try {
            testDataFrame = new RDataFrame(engine, names, rowNames, data,
                    "test");

            Assert.assertEquals(3,testDataFrame.dim()[1]);
            Assert.assertEquals(2,testDataFrame.dim()[2]);
        } catch (RException eee) {
            Assert.fail();
        }

        
    }
}
