/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.j2r;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.j2r.types.RList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ListTest {

    private static Log log = LogFactory.getLog(DataframeTest.class);

    private REngine engine;

    @BeforeClass
    public static void tryREnv() {
        try {
            new RProxy();
        } catch (RException eee) {
            if (log.isErrorEnabled()) {
                log.error("No R environment found to run tests. Skip");
            }
            Assume.assumeTrue(false);
        }
    }

    @Before
    public void setUp() throws Exception {
        engine = new RProxy();
    }

    @After
    public void tearDown() throws Exception {
        if (engine != null) {
            engine.terminate();
        }
    }

    @Test
    public void testRList1() throws Exception {
        /*
         * This test list creation method 1 and data assignment : 
         * RList(REngine) + setData
         */

        RList list1 = new RList(engine);
        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);
        list1.setVariable("a");
        list1.setData(data);
        assertEquals(1, engine.eval("a[[1]]"));
        assertEquals("toto", engine.eval("a[[2]]"));
        assertEquals(3.6, engine.eval("a[[3]]"));
        assertEquals(true, engine.eval("a[[4]]"));
    }

    @Test
    public void testRList2() throws Exception {
        /*
         * This test list creation method2
         */

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(names, data, engine, "a");

        assertEquals(1, engine.eval("a[[1]]"));
        assertEquals("toto", engine.eval("a[[2]]"));
        assertEquals(3.6, engine.eval("a[[3]]"));
        assertEquals(true, engine.eval("a[[4]]"));

        assertEquals("a", engine.eval("names(a)[1]"));
        assertEquals("b", engine.eval("names(a)[2]"));
        assertEquals("c", engine.eval("names(a)[3]"));
        assertEquals("d", engine.eval("names(a)[4]"));
    }

    @Test
    public void testGetNames() throws Exception {

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(names, data, engine, "a");

        assertEquals("a", list1.getNames().get(0));
        assertEquals("b", list1.getNames().get(1));
        assertEquals("c", list1.getNames().get(2));
        assertEquals("d", list1.getNames().get(3));

        engine.voidEval("a<-list(a=as.integer(1),b=\"toto\",c=3.6,d=TRUE,e=FALSE)");
        try {
            list1.getNames();
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }

    }

    @Test
    public void testGetName() throws Exception {
        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(names, data, engine, "a");

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        try {
            list1.getName(4);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }
    }

    @Test
    public void testSetNames() throws RException {
        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setData(data);
        list1.setNames(names);

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");
        names.add("tooLongNow");

        try {
            list1.setNames(names);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }
    }

    @Test
    public void testSetName() throws Exception {
        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setData(data);
        list1.setName(3, "d");
        list1.setName(0, "a");
        list1.setName(1, "b");
        list1.setName(2, "c");

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        list1.setName(3, "d");
        assertEquals("d", list1.getName(3));

        try {
            list1.setName(7, "badName");
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }
    }

    @Test
    public void testSetGetEngine() throws Exception {

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setData(data);
        list1.setName(0, "a");
        list1.setName(1, "b");
        list1.setName(2, "c");
        list1.setName(3, "d");

        REngine secondEngine = new RProxy();
        list1.setEngine(secondEngine);

        //Test that the values are still OK
        assertEquals(1, engine.eval("a[[1]]"));
        assertEquals("toto", engine.eval("a[[2]]"));
        assertEquals(3.6, engine.eval("a[[3]]"));
        assertEquals(true, engine.eval("a[[4]]"));

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        //Check that the engine is not the old engine
        Assert.assertNotSame(engine, list1.getEngine());

        //Check that the engine is the new one
        Assert.assertEquals(secondEngine, list1.getEngine());
    }

    @Test
    public void testSetGetVariable() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        Assert.assertEquals("a", list1.getVariable());
    }

    @Test
    public void testSetIntDouble() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, 3.6);
        assertEquals(3.6, engine.eval("a[[4]]"));
        //test if everything is ok is there is another item at index
        list1.set(3, 4.5);
        assertEquals(4.5, engine.eval("a[[4]]"));
    }

    @Test
    public void testSetIntBoolean() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, true);
        assertEquals(true, engine.eval("a[[4]]"));
        //test if everything is ok is there is another item at index, test if false works
        list1.set(3, false);
        assertEquals(false, engine.eval("a[[4]]"));
    }

    @Test
    public void testSetIntString() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, "titi");
        assertEquals("titi", engine.eval("a[[4]]"));
        //test if everything is ok is there is another item at index
        list1.set(3, "toto");
        assertEquals("toto", engine.eval("a[[4]]"));
    }

    @Test
    public void testSetIntInt() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, 4);
        assertEquals(4, engine.eval("a[[4]]"));
        //test if everything is ok is there is another item at index
        list1.set(3, 6);
        assertEquals(6, engine.eval("a[[4]]"));
    }

    @Test
    public void testSetIntREXP() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(0, 3.6);
        list1.set(1, 3);
        list1.set(2, true);
        list1.set(3, "titi");

        RList list3 = new RList(engine);
        list3.setVariable("r");
        list3.set(0, 3.6);
        list3.set(1, 3);
        list3.set(2, true);
        list3.set(3, "titi");

        RList list2 = new RList(engine);
        list2.setVariable("zerty");
        list2.set(1, list1);
        list2.set(2, "a");

        assertEquals(3.6, engine.eval("zerty[[2]][[1]]"));
        assertEquals(3, engine.eval("zerty[[2]][[2]]"));
        assertEquals(true, engine.eval("zerty[[2]][[3]]"));
        assertEquals("titi", engine.eval("zerty[[2]][[4]]"));
        assertEquals("a", engine.eval("zerty[[3]]"));

        list2.set(2, list3);
        assertEquals(3.6, engine.eval("zerty[[2]][[1]]"));
        assertEquals(3, engine.eval("zerty[[2]][[2]]"));
        assertEquals(true, engine.eval("zerty[[2]][[3]]"));
        assertEquals("titi", engine.eval("zerty[[2]][[4]]"));
        assertEquals(3.6, engine.eval("zerty[[3]][[1]]"));
        assertEquals(3, engine.eval("zerty[[3]][[2]]"));
        assertEquals(true, engine.eval("zerty[[3]][[3]]"));
        assertEquals("titi", engine.eval("zerty[[3]][[4]]"));

    }

    @Test
    public void testGet() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(0, 3.6);
        list1.set(1, 3);
        list1.set(2, true);
        list1.set(3, "titi");
        assertEquals(3.6, list1.get(0));
        assertEquals(3, list1.get(1));
        assertEquals(true, list1.get(2));
        assertEquals("titi", list1.get(3));
        assertEquals(3.6, engine.eval("a[[1]]"));
        assertEquals(3, engine.eval("a[[2]]"));
        assertEquals(true, engine.eval("a[[3]]"));
        assertEquals("titi", engine.eval("a[[4]]"));

        try {
            list1.get(45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }
    }

    @Test
    public void testGetFromGetData() throws Exception {
        //test getting a list with double, integer, string and boolean
        RList list1 = new RList(engine);
        engine.voidEval("a<-list(a=as.integer(1),b=\"toto\",c=3.6,d=TRUE)");
        list1.getFrom("a");

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        assertEquals(1, list1.getData().get(0));
        assertEquals("toto", list1.getData().get(1));
        assertEquals(3.6, list1.getData().get(2));
        assertEquals(true, list1.getData().get(3));

        //test if names, data and attributes are null (same list)
        // force a null List<String> to use the right constructor (and not the
        //one with String[] parameter)
        List<String> name= null;
        RList list2 = new RList(name, null, engine, "ble");
        list2.setAttributes(null);
        list2.getFrom("a");

        assertEquals("a", list2.getName(0));
        assertEquals("b", list2.getName(1));
        assertEquals("c", list2.getName(2));
        assertEquals("d", list2.getName(3));

        assertEquals(1, list2.getData().get(0));
        assertEquals("toto", list2.getData().get(1));
        assertEquals(3.6, list2.getData().get(2));
        assertEquals(true, list2.getData().get(3));
    }

    @Test
    public void testSetGetAttribute() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setAttribute("stringAttribute", "test_value");
        list1.setAttribute("doubleAttribute", 3.6);
        list1.setAttribute("integerAttribute", 3);
        list1.setAttribute("booleanTrueAttribute", true);
        list1.setAttribute("booleanFalseAttribute", false);        
        Boolean[] booleanArray = { true, false };
        list1.setAttribute("badAttribute", booleanArray);

        Assert.assertEquals("test_value", engine
                .eval("attributes(a)$stringAttribute"));
        Assert.assertEquals(3.6, engine.eval("attributes(a)$doubleAttribute"));
        Assert.assertEquals(3, engine.eval("attributes(a)$integerAttribute"));
        Assert.assertEquals(true, engine
                .eval("attributes(a)$booleanTrueAttribute"));
        Assert.assertEquals(false, engine
                .eval("attributes(a)$booleanFalseAttribute"));
        Assert
                .assertEquals("test_value", list1
                        .getAttribute("stringAttribute"));
        Assert.assertEquals(3.6, list1.getAttribute("doubleAttribute"));
        Assert.assertEquals(3, list1.getAttribute("integerAttribute"));
        Assert.assertEquals(true, list1.getAttribute("booleanTrueAttribute"));
        Assert.assertEquals(false, list1.getAttribute("booleanFalseAttribute"));

        //Try resetting attributes to see everything goes well when attribute already exists.
        list1.setAttribute("stringAttribute", "test_value");
        list1.setAttribute("doubleAttribute", 3.6);
        list1.setAttribute("integerAttribute", 3);
        list1.setAttribute("booleanTrueAttribute", true);
        list1.setAttribute("booleanFalseAttribute", false);

        Assert.assertEquals("test_value", engine
                .eval("attributes(a)$stringAttribute"));
        Assert.assertEquals(3.6, engine.eval("attributes(a)$doubleAttribute"));
        Assert.assertEquals(3, engine.eval("attributes(a)$integerAttribute"));
        Assert.assertEquals(true, engine
                .eval("attributes(a)$booleanTrueAttribute"));
        Assert.assertEquals(false, engine
                .eval("attributes(a)$booleanFalseAttribute"));
        Assert
                .assertEquals("test_value", list1
                        .getAttribute("stringAttribute"));
        Assert.assertEquals(3.6, list1.getAttribute("doubleAttribute"));
        Assert.assertEquals(3, list1.getAttribute("integerAttribute"));
        Assert.assertEquals(true, list1.getAttribute("booleanTrueAttribute"));
        Assert.assertEquals(false, list1.getAttribute("booleanFalseAttribute"));

        //Try if exception normally thrown when attribute does not exist
        list1.setAttributes(new HashMap<String, Object>());
        engine.eval("attr(a,\"BooleanFalseAttribute\")<-FALSE");
        Assert.assertEquals(false, list1.getAttribute("booleanFalseAttribute"));

        try {
            list1.getAttribute("toto");
            Assert.fail("Should have thrown an RException at this point");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);

        }
    }

    @Test
    public void testSetGetAttributes() throws Exception {
        RList list1 = new RList(engine);
        list1.setVariable("a");

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("stringAttribute", "test_value");
        attributes.put("doubleAttribute", 3.6);
        attributes.put("integerAttribute", 3);
        attributes.put("booleanTrueAttribute", true);
        attributes.put("booleanFalseAttribute", false);
        Boolean[] booleanArray = { true, false };
        attributes.put("badAttribute", booleanArray);
        list1.setAttributes(attributes);

        engine.eval("attr(a,\"newBooleanFalseAttribute\")<-FALSE");

        Assert.assertEquals("test_value", engine
                .eval("attributes(a)$stringAttribute"));
        Assert.assertEquals(3.6, engine.eval("attributes(a)$doubleAttribute"));
        Assert.assertEquals(3, engine.eval("attributes(a)$integerAttribute"));
        Assert.assertEquals(true, engine
                .eval("attributes(a)$booleanTrueAttribute"));
        Assert.assertEquals(false, engine
                .eval("attributes(a)$booleanFalseAttribute"));
        Assert.assertEquals("test_value", list1.getAttributes().get(
                "stringAttribute"));
        Assert.assertEquals(3.6, list1.getAttributes().get("doubleAttribute"));
        Assert.assertEquals(3, list1.getAttributes().get("integerAttribute"));
        Assert.assertEquals(true, list1.getAttributes().get(
                "booleanTrueAttribute"));
        Assert.assertEquals(false, list1.getAttributes().get(
                "booleanFalseAttribute"));
        Assert.assertEquals(false, list1.getAttributes().get(
                "newBooleanFalseAttribute"));
    }

    @Test
    public void testRList1WithoutAutoCommit() throws Exception {
        /*
         * This test list creation method 1 and data assignment : 
         * RList(REngine) + setData
         */

        engine.setAutoCommit(false);

        RList list1 = new RList(engine);
        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);
        list1.setVariable("a");
        list1.setData(data);
        engine.setAutoCommit(true);
        assertEquals(1, engine.eval("a[[1]]"));
        assertEquals("toto", engine.eval("a[[2]]"));
        assertEquals(3.6, engine.eval("a[[3]]"));
        assertEquals(true, engine.eval("a[[4]]"));
    }

    @Test
    public void testRList2WithoutAutoCommit() throws Exception {
        /*
         * This test list creation method2
         */

        engine.setAutoCommit(false);

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(names, data, engine, "a");

        engine.setAutoCommit(true);

        assertEquals(1, engine.eval("a[[1]]"));
        assertEquals("toto", engine.eval("a[[2]]"));
        assertEquals(3.6, engine.eval("a[[3]]"));
        assertEquals(true, engine.eval("a[[4]]"));

        assertEquals("a", engine.eval("names(a)[1]"));
        assertEquals("b", engine.eval("names(a)[2]"));
        assertEquals("c", engine.eval("names(a)[3]"));
        assertEquals("d", engine.eval("names(a)[4]"));
    }

    @Test
    public void testGetNamesWithoutAutoCommit() throws Exception {

        engine.setAutoCommit(false);

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(names, data, engine, "a");

        assertEquals("a", list1.getNames().get(0));
        assertEquals("b", list1.getNames().get(1));
        assertEquals("c", list1.getNames().get(2));
        assertEquals("d", list1.getNames().get(3));

        engine.setAutoCommit(true);
    }

    @Test
    public void testGetNameWithoutAutoCommit() throws Exception {

        engine.setAutoCommit(false);

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(names, data, engine, "a");

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        try {
            list1.getName(4);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }

        engine.setAutoCommit(true);
    }

    @Test
    public void testSetNamesWithoutAutoCommit() throws RException {

        engine.setAutoCommit(false);

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        List<String> names = new ArrayList<String>();
        names.add("a");
        names.add("b");
        names.add("c");
        names.add("d");

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setData(data);
        list1.setNames(names);

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        engine.setAutoCommit(true);
    }

    @Test
    public void testSetNameWithoutAutoCommit() throws Exception {

        engine.setAutoCommit(false);

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setData(data);
        list1.setName(0, "a");
        list1.setName(1, "b");
        list1.setName(2, "c");
        list1.setName(3, "d");

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        engine.setAutoCommit(true);
    }

    @Test
    public void testSetGetEngineWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);

        List<Object> data = new ArrayList<Object>();
        data.add(1);
        data.add("toto");
        data.add(3.6);
        data.add(true);

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setData(data);
        list1.setName(0, "a");
        list1.setName(1, "b");
        list1.setName(2, "c");
        list1.setName(3, "d");

        REngine secondEngine = new RProxy();
        list1.setEngine(secondEngine);

        //Test that the values are still OK
        assertEquals(1, engine.eval("a[[1]]"));
        assertEquals("toto", engine.eval("a[[2]]"));
        assertEquals(3.6, engine.eval("a[[3]]"));
        assertEquals(true, engine.eval("a[[4]]"));

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        //Check that the engine is not the old engine
        Assert.assertNotSame(engine, list1.getEngine());

        //Check that the engine is the new one
        Assert.assertEquals(secondEngine, list1.getEngine());

        engine.setAutoCommit(true);
    }

    @Test
    public void testSetGetVariableWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");
        Assert.assertEquals("a", list1.getVariable());
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetIntDoubleWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, 3.6);
        assertEquals(3.6, engine.eval("a[[4]]"));
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetIntBooleanWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, true);
        assertEquals(true, engine.eval("a[[4]]"));
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetIntStringWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, "titi");
        assertEquals("titi", engine.eval("a[[4]]"));
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetIntIntWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(3, 4);
        assertEquals(4, engine.eval("a[[4]]"));
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetIntREXPWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(0, 3.6);
        list1.set(1, 3);
        list1.set(2, true);
        list1.set(3, "titi");

        RList list2 = new RList(engine);
        list2.setVariable("zerty");
        list2.set(0, list1);
        list2.set(1, "a");

        engine.setAutoCommit(true);

        assertEquals(3.6, engine.eval("zerty[[1]][[1]]"));
        assertEquals(3, engine.eval("zerty[[1]][[2]]"));
        assertEquals(true, engine.eval("zerty[[1]][[3]]"));
        assertEquals("titi", engine.eval("zerty[[1]][[4]]"));
        assertEquals("a", engine.eval("zerty[[2]]"));
    }

    @Test
    public void testGetWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.set(0, 3.6);
        list1.set(1, 3);
        list1.set(2, true);
        list1.set(3, "titi");
        assertEquals(3.6, list1.get(0));
        assertEquals(3, list1.get(1));
        assertEquals(true, list1.get(2));
        assertEquals("titi", list1.get(3));
        engine.setAutoCommit(true);

        try {
            list1.get(45);
            Assert.fail("Error was not thrown although it should have been");
        } catch (IndexOutOfBoundsException eee) {
        }
    }

    @Test
    public void testGetFromGetDataWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        engine.voidEval("a<-list(a=as.integer(1),b=\"toto\",c=3.6,d=TRUE)");
        list1.getFrom("a");

        assertEquals("a", list1.getName(0));
        assertEquals("b", list1.getName(1));
        assertEquals("c", list1.getName(2));
        assertEquals("d", list1.getName(3));

        assertEquals(1, list1.getData().get(0));
        assertEquals("toto", list1.getData().get(1));
        assertEquals(3.6, list1.getData().get(2));
        assertEquals(true, list1.getData().get(3));

        engine.setAutoCommit(true);
    }

    @Test
    public void testSetGetAttributeWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);

        RList list1 = new RList(engine);
        list1.setVariable("a");
        list1.setAttribute("stringAttribute", "test_value");
        list1.setAttribute("doubleAttribute", 3.6);
        list1.setAttribute("integerAttribute", 3);
        list1.setAttribute("booleanTrueAttribute", true);
        list1.setAttribute("booleanFalseAttribute", false);

        Assert
                .assertEquals("test_value", list1
                        .getAttribute("stringAttribute"));
        Assert.assertEquals(3.6, list1.getAttribute("doubleAttribute"));
        Assert.assertEquals(3, list1.getAttribute("integerAttribute"));
        Assert.assertEquals(true, list1.getAttribute("booleanTrueAttribute"));
        Assert.assertEquals(false, list1.getAttribute("booleanFalseAttribute"));

        list1.setAttribute("test_attribute", "third_value");
        Assert.assertEquals("third_value",
                engine.eval("attributes(a)$test_attribute"));

        try {
            list1.getAttribute("toto");
            Assert.fail("Should have thrown an RException at this point");
        } catch (RException eee) {
            log.debug("Exception normally thrown ", eee);

        }
        engine.setAutoCommit(true);
    }

    @Test
    public void testSetGetAttributesWithoutAutoCommit() throws Exception {
        engine.setAutoCommit(false);
        RList list1 = new RList(engine);
        list1.setVariable("a");

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("stringAttribute", "test_value");
        attributes.put("doubleAttribute", 3.6);
        attributes.put("integerAttribute", 3);
        attributes.put("booleanTrueAttribute", true);
        attributes.put("booleanFalseAttribute", false);
        list1.setAttributes(attributes);

        Assert.assertEquals("test_value",
                list1.getAttributes().get("stringAttribute"));
        Assert.assertEquals(3.6, list1.getAttributes().get("doubleAttribute"));
        Assert.assertEquals(3, list1.getAttributes().get("integerAttribute"));
        Assert.assertEquals(true,
                list1.getAttributes().get("booleanTrueAttribute"));
        Assert.assertEquals(false,
                list1.getAttributes().get("booleanFalseAttribute"));
        engine.setAutoCommit(true);
    }





}
