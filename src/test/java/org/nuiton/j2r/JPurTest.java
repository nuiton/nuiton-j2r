/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
* JPurTest.java
*
* Created: 25 août 06
*
* @author Arnaud Thimel <thimel@codelutin.com>
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.j2r;

import static org.nuiton.j2r.TestConstants.*;
import junit.framework.TestCase;


public class JPurTest extends TestCase {

    public void testSimpleOp() throws Exception {
        LutinTimer lt = new LutinTimer();
        for (int loop=0; loop<S_NB_LOOPS; loop++) {
            int t=0;
            lt.startTiming();
            while (t<S_T_MAX) {
                t += 2;
                t *= 2;
                t /= 2;
            }
            lt.endTiming();
        }
        double[] results = lt.computeResults();
        System.err.println("[SO]min: " + results[0]);
        System.err.println("[SO]avg: " + results[1]);
        System.err.println("[SO]max: " + results[2]);
        System.err.println("[SO]etype: " + results[3]);
    }

    public void testVector() throws Exception {
        LutinTimer lt = new LutinTimer();
        for (int loop=0; loop<V_NB_LOOPS; loop++) {
            lt.startTiming();
            double[] a = new double[V_MAX];
            double[] b = new double[V_MAX];
            for (int i=0; i<a.length; i++) {
                a[i] = (i + 0.5);
                b[i] = ((V_MAX-0.5)-i);
            }
            
            double[] r = new double[V_MAX];
            for (int i=0; i<a.length; i++) {
                r[i] = a[i] * b[i];
            }
            lt.endTiming();
        }
        double[] results = lt.computeResults();
        System.err.println("[V]min: " + results[0]);
        System.err.println("[V]avg: " + results[1]);
        System.err.println("[V]max: " + results[2]);
        System.err.println("[V]etype: " + results[3]);
    }

}
