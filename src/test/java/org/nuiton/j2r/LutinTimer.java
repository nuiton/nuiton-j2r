/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
* Timer.java
*
* Created: 18 août 2006
*
* @author Arnaud Thimel <thimel@codelutin.com>
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.j2r;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LutinTimer {

    private static Log log = LogFactory.getLog(LutinTimer.class);

    private List<Long> times = new ArrayList<Long>();
    private long currentStart = -1;

    public void startTiming() {
        currentStart = System.currentTimeMillis();
    }

    public long endTiming() {
        if (currentStart == -1) {
            System.err.println("Timer didn't start...");
            return -1;
        }
        long end = System.currentTimeMillis();
        long duration = end - currentStart;
        currentStart = -1;
        times.add(duration);
        return duration;
    }

    public void markTiming() {
        if (currentStart == -1) {
            startTiming();
            return;
        }
        long end = System.currentTimeMillis();
        long duration = end - currentStart;
        currentStart = end;
        times.add(duration);
    }

    public void reset() {
        currentStart = -1;
        times.clear();
    }

    public double[] computeResults() {
        if (times.size() == 0) {
            System.err.println("To time saved");
            return null;
        }
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        long total = 0;
        for (long time : times ){
            if (time > max) {
                max = time;
            }
            if (time < min) {
                min = time;
            }
            total += time;
        }
        double avg = (((double)total) / times.size());
        double etype = 0;
        for (long time : times) {
            etype += Math.pow(((double)time) - avg, 2);
        }
        etype /= times.size();
        etype = Math.sqrt(etype);
        double[] results = new double[4];
        results[0] = min;
        results[1] = avg;
        results[2] = max;
        results[3] = etype;
        return results;
    }

} //Timer
