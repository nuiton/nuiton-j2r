/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
 * NetTest.java
 *
 * Created: 23 août 06
 *
 * @author Arnaud Thimel <thimel@codelutin.com>
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */
package org.nuiton.j2r;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.nuiton.j2r.jni.RJniEngine;

public class JNITest extends AbstractEngineTest {

    private static Log log = LogFactory.getLog(JNITest.class);

    @BeforeClass
    public static void tryREnv() {

        RProxy testedEngine = null;

        //Force R.type to net
        String savedRType = System.getProperty("R.type", "");
        System.setProperty("R.type", "jni");

        try {
            testedEngine = new RProxy();
        } catch (RException eee) {
            if (log.isErrorEnabled()) {
                log.error("No R environment found to run tests. Skip");
            }
            Assume.assumeTrue(false);
        }

        //Restore old R.type
        System.setProperty("R.type", savedRType);

        if (testedEngine != null) {
            Assume.assumeTrue(testedEngine.isJni());
            if (log.isErrorEnabled()) {
                log.error("No JRI environment found. Skip tests.");
            }
        }


    }

    @Before
    public void setUp() throws Exception {
        LutinTimer init = new LutinTimer();
        init.startTiming();
        savedRType = System.getProperty("R.type", "");
        System.setProperty("R.type", "jni");
        if (engine == null) {
            try {
                engine = new RProxy();
            } catch (RException eee){
                Assume.assumeTrue(false);
            }
        }
        if (log.isInfoEnabled()) {
            log.info("jni init: " + init.endTiming() + "ms");
        }

        Assume.assumeTrue(engine instanceof RJniEngine);
    }

    @After
    public void tearDown() throws Exception {
        if (engine !=null){
            engine.terminate();
        }
        System.setProperty("R.type", savedRType);
    }
    
} // JNITest
