/*
 * #%L
 * Nuiton Java-2-R
 * %%
 * Copyright (C) 2006 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/* *
* TestConstants.java
*
* Created: 25 août 06
*
* @author Arnaud Thimel <thimel@codelutin.com>
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.j2r;

import org.junit.Ignore;

@Ignore
public class TestConstants {

    /*
     * Constants for simple operations
     */
    public static final int S_T_MAX = 100000;

    public static final String S_OP = "while (t<" + S_T_MAX + ") {t<-t+2; t<-t*2; t<-t/2;}";

    public static final int S_NB_LOOPS = 5;

    /*
     * Constants for volumetric operations
     */
    public static final int V_MAX = 50000;

    public static final String V_OP_A = "a<-0.5:"+(V_MAX-0.5);
    public static final String V_OP_B = "b<-"+(V_MAX-0.5)+":0.5";
    public static final String V_OP_AB = "r<-a*b";
    public static final String V_OP_R = V_OP_A + "; " + V_OP_B + "; " + V_OP_AB;

    public static final int V_NB_LOOPS = 10;

} //TestConstants
